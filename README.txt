-------
License
-------

This program is free software: you can redistribute it and/or modify it under the terms 
of the GNU General Public License Version 3, as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the GNU General Public License for more details (https://www.gnu.org/licenses).

------
Papers
------

This tool was published in the demo track of the AAMAS conference 
(International Conference on Autonomous Agents and Multiagent Systems):
http://dl.acm.org/citation.cfm?id=3091448

The implementation is based on the algorithms published in the
paper 'Realisability of Production Recipes' (ECAI 2016): 
http://eprints.nottingham.ac.uk/35424/

-----
Video
-----

A video of the tool with the `old' command-line option (which runs the old algorithm)
can be viewed here: https://www.youtube.com/watch?v=SEveuikI3p8

------------------------------
Compiling and running the tool
------------------------------

To compile the source code, type the following from the root folder (Linux/Mac only):

./make

This will create a JAR file. Run the JAR file with the following command:

java -jar WorkOrder.jar

Running the JAR file with the following command will use the old topology-computation algorithm:

java -jar WorkOrder.jar old

---------------
Files & folders
---------------

admin/  
	contains the MANIFEST.MF file for creating the JAR, and a file with
	a list of known bugs and features that are yet to be implemented.
	
classes/  
	contains the compiled Java classes.

data/
	contains some example models (recipes and resources), and some example
	B2MML templates from which B2MML operations-requests and operations-schedules 
	could be created.

Default.theme 
	is a configuration file that is used to set the Java look and feel.

libs/  
	contains JAR files that are used by the tool.	

make 
	creates the JAR file.

src/  
	contains all the Java source code.

WorkOrder.jar
	is created on compiling the source code via the Makefile.
	
README.txt  
	is what you are reading.


------------------------------------
Current developer(s) / maintainer(s)
------------------------------------

Lavindra de Silva [lavindra.desilva@eng.cam.ac.uk]
