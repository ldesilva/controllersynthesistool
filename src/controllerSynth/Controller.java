package controllerSynth;
//package com.mxgraph.examples.swing;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.event.*;
import javax.swing.text.BadLocationException;
//import java.awt.event.ActionListener;
//import java.awt.event.ActionEvent;
import java.awt.*;
import java.io.*;
//import java.awt.event.FocusEvent;
//import java.awt.event.FocusListener;
import java.util.*;

import com.mxgraph.swing.handler.mxRubberband;
import com.mxgraph.util.mxEvent;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxEventObject;
import com.mxgraph.util.mxEventSource.mxIEventListener;

import javax.swing.JFrame;

import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.*;
import com.mxgraph.model.*;
import java.util.List;

import com.mxgraph.layout.hierarchical.*;



public class Controller extends JFrame
{
	
	/**
	 * 
	 */
	//private static final long serialVersionUID = -2764911804288120883L;
	//private static final String circle = "shape=ellipse;perimeter=ellipsePerimeter";
	//private static final String B2MMLfolder = "B2MML/";
	//private static final String defaultVertex = "State";
	////private static final String NOP = "nop";
	//private static final String defaultEdge = Constants.NOP; 
	//private static final String TITLE = "Work Order Development Toolkit"; 
	//private static String b2mmlViewer = "B2MML Viewer"; 
	//private static final String INSYNC = "in:"; 
	//private static final String OUTSYNC = "out:"; 
	//private static final String IN_OUT_COLOR = "fontColor=#DC143C"; 
	//private static final String NOP_COLOR = "fontColor=#00C5CD"; 
	//private static final String DEFAULT_COLOR = "fontColor=#000000"; 
	
	final mxGraphComponent graphComponent;
        private mxHierarchicalLayout layout;
	private int defaultEdgeCounter = 0;
	private int defaultVertexCounter = 0; 
        private java.util.Date timer;
 	private Object parent = null;
	//private Set<String> vertices = new HashSet<String>();
	//private Set<String> labels = new HashSet<String>();
	private Set<ControllerTransition> contEdges = new HashSet<ControllerTransition>();
	private mxGraph graph;
	private int resourceCount;
	private int yCoord = 20;
        private JLabel manufButton;
        private JFrame buttons;
        private boolean keepGraphsClipped = false;


	public Controller(Set<ControllerTransition> edges, JLabel manButton, int resCount)
	{
		super(Constants.TITLE);

                buttons = new JFrame();

                JLabel infoPanelLabel = new JLabel(Constants.ALERTS_GUI_LABEL);
                buttons.add(infoPanelLabel);

                buttons.setLayout(new FlowLayout());
                this.setLayout(new BorderLayout());
                this.add(buttons.getContentPane(),BorderLayout.SOUTH);

		contEdges = edges;
		resourceCount = resCount;
                manufButton = manButton;

		graph = new mxGraph();

    		Map<String, Object> style = graph.getStylesheet().getDefaultEdgeStyle();
    		style.put(mxConstants.STYLE_ROUNDED, true);
    		style.put(mxConstants.STYLE_EDGE, mxConstants.EDGESTYLE_ENTITY_RELATION);

		parent = graph.getDefaultParent();

		graph.setStylesheet(Helper.createStylesheet(graph));
		graph.setAllowLoops(true);
		//graph.setLabelsClipped(true);
		graph.getModel().beginUpdate();

                try
                {
                	java.util.List<Object> topState = new ArrayList<Object>();
                        for(int i=0; i<resourceCount; i++) { 
				topState.add("state0");
       			}

			int x = 20;
			//int y = 20;
			Set<Object> visited = new HashSet<Object>();

			SimulationTriple contState = new SimulationTriple(
								topState,
								"state0", // recipe state
								new ResourceVector());

                        Object v1 = graph.insertVertex(parent, 
                                                       null, 
                                                       //topState.toString().replaceAll("state", "s"), 
                                                       contState.toString(resCount), 
                                                       x,     
                                                       yCoord,     
						       //contState.toString(resCount).length() * 
						       topState.toString().length() * 
						       Constants.TOPOLOGY_LABEL_SIZE_RATIO * 
						       Constants.TOPOLOGY_VERTEX_FONTSIZE,
                                                       //topState.toString().length() * Constants.WIDTH_PER_CHAR,    
						       topState.toString().length() * 2.5,
                                                       //Constants.CONTROLLER_VERTEX_HEIGHT,    
                                                       Constants.TOPOLOGY_VERTEX_STYLE);
			visited.add(v1);
			drawCont(v1, x, visited, resCount);
                }

		finally { graph.getModel().endUpdate(); }

                layout = new mxHierarchicalLayout(graph,SwingConstants.WEST);
                layout.setFineTuning(true);
                layout.setInterRankCellSpacing(500);
                layout.setIntraCellSpacing(250);
                layout.execute(parent);

                graph.setAllowDanglingEdges(false);
                //graph.setDefaultOverlap(0.9);
		graph.setEdgeLabelsMovable(false);


		graphComponent = new mxGraphComponent(graph);
                clipLabels();

                graph.getModel().addListener(mxEvent.CHANGE, new mxIEventListener() {
                	public void invoke(Object sender, mxEventObject evt) {	
				/** temporarily removing 
                                layout = new mxHierarchicalLayout(graph,SwingConstants.WEST);
                                layout.setFineTuning(true);
                                layout.setInterRankCellSpacing(500);
                                layout.setIntraCellSpacing(250);
                                layout.execute(parent);

                                clipLabels();
				**/
                	}
                });

                // Initial validation
                graphComponent.validateGraph();
		
                new mxRubberband(graphComponent);
		getContentPane().add(graphComponent);
		graphComponent.getGraphControl().setFocusable(true);

		graphComponent.getGraphControl().addMouseListener(new MouseAdapter()
		{
			Object v;	
			public void mouseReleased(MouseEvent e) { }

		});
	}

        public JFrame getButtons() {
                return buttons;
        }



        public void clipLabels(boolean clip) {
                keepGraphsClipped = clip;
                clipLabels();
        }

        public void clipLabels() {
                graph.setLabelsClipped(keepGraphsClipped);
		graphComponent.refresh();
        }


        public void zoomIn() {
                graphComponent.zoomIn();
        }

        public void zoomOut() {
                graphComponent.zoomOut();
        }



        private void drawCont(Object v1, int x, Set<Object> visited, int resCount) {

		double newX = 0;

		boolean firstTimeInLoop = true;

                for (ControllerTransition contEdge : contEdges) {
			String src = contEdge.source.toString(resCount);
                        if(src.equals(graph.getLabel(v1))) {

				boolean alreadyVisitedv2 = Helper.vtxIsIn(contEdge.target.toString(), visited);
				Object v2;
               			String edgeLab = "";
 
				for (List<Object> labSeq : contEdge.topologyLabelSequence) {	
	
					edgeLab += "[";
					boolean firstTime = true;

					for (Object lab : labSeq) {

						if(!firstTime) {
							edgeLab += ", "; 
						}
							
						edgeLab += lab.toString();
						firstTime = false;
					}

					edgeLab += "]\n";
				}

				String edgeLabel = "\n\n"+contEdge.opExpr.label + "\n|\n" + edgeLab;

				//String edgeLabel = contEdge.opExpr.label + "\n" + 
				//		   contEdge.topologyLabelSequence.toString();

				if(!alreadyVisitedv2) {
		
					if(contEdge.opExpr.label.length() > 
					   contEdge.topologyLabelSequence.toString().length()) {

						newX = x + (contEdge.opExpr.label.length() * 
							    Constants.CONTROLLER_EDGE_RATIO * 
							    Constants.TOPOLOGY_EDGE_FONTSIZE);
					}
					else {
						newX = x + (contEdge.topologyLabelSequence.toString().length() *
							    Constants.CONTROLLER_EDGE_RATIO * 
							    Constants.TOPOLOGY_EDGE_FONTSIZE);
					}

                                	if(!firstTimeInLoop) {
                                        	yCoord += Constants.CONTROLLER_Y_OFFSET;
                                	}

                                	firstTimeInLoop = false;
					int length;

					if(contEdge.target.getRVasStr(resCount).toString().length() > 
					   contEdge.target.topologyState.toString().length()) {
						length = contEdge.target.getRVasStr(resCount).toString().length();
					}
					else {
						length = contEdge.target.topologyState.toString().length();
					}

 
                        		v2 = graph.insertVertex(parent, 
							       null, 
							       contEdge.target.toString(resCount), 
							       (int)newX, 
							       yCoord, 
							       //contEdge.target.toString(resCount).length() * 
							       length * 
							       Constants.TOPOLOGY_LABEL_SIZE_RATIO * 
							       Constants.TOPOLOGY_VERTEX_FONTSIZE, 
							       length * 2.5,
							       //Constants.CONTROLLER_VERTEX_HEIGHT, 
							       Constants.TOPOLOGY_VERTEX_STYLE);
				}
				else {
					v2 = Helper.getVertex(contEdge.target.toString(resCount), visited);
				}
				
				
                        	graph.insertEdge(parent, null, edgeLabel, v1, v2, Constants.TOPOLOGY_EDGE_STYLE);

				if(!alreadyVisitedv2) {
					visited.add(v2);		
					drawCont(v2, (int)newX, visited, resCount);
				}
                        }
                }       
        }
}
