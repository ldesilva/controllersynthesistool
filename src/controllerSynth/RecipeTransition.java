package controllerSynth;

public class RecipeTransition {

	public String source;
        public OperationExpression label;
        public String target;

        public RecipeTransition(String a, OperationExpression b, String c) {
        	this.source = a;
                this.label = b;
                this.target = c;
        }
}

