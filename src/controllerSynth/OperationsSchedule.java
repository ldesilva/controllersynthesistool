package controllerSynth;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.event.*;
import javax.swing.text.BadLocationException;
import java.awt.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.util.List;

import com.mxgraph.swing.handler.mxRubberband;
import com.mxgraph.util.mxEvent;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxEventObject;
import com.mxgraph.util.mxEventSource.mxIEventListener;

import javax.swing.JFrame;

import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.*;
import com.mxgraph.model.*;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.*;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import java.io.File;




/*
 * This class creates a B2MML operations-schedule from the
 * controller that was created on checking manufacturability.
 * An operations-schedule is a Java 'Document' (XML) object, 
 * which could be sent to a manufacturing execution system 
 * (MES) for execution.
 */

public class OperationsSchedule {
	
	private Set<ControllerTransition> contEdges = new HashSet<ControllerTransition>();
	private int resourceCount;
	private Document operationsSchedule = null;
	private String operationsScheduleAsStr = null;
        private DocumentBuilderFactory dbFactory = null;
        private DocumentBuilder dBuilder = null;
	private static final String CHOICE_PT_STR = "ChoicePoint";
	private static final String SEQUENCE_STR = "Sequence";
	private static final String REDUNDANT_STR = "redundant";
	private static final int EMPTY = -1;




	public OperationsSchedule(Set<ControllerTransition> edges, int resCount)
	{

		// Save the controller and number 
		// of available resources
		contEdges = edges;
		resourceCount = resCount;

		// This variable is used to create
		// the topology's initial state, which
		// is the combination of the initial
		// state of each resource
               	java.util.List<Object> topState = new ArrayList<Object>();

		try {

			// Create a new XML (B2MML) document
         		dbFactory = DocumentBuilderFactory.newInstance();
         		dBuilder = dbFactory.newDocumentBuilder();
         		operationsSchedule = dBuilder.newDocument();	

			// Create the initial XML/B2MML skeleton, 
			// and add today's date as an element
			initB2MML(operationsSchedule);
		}
		catch(Exception e) {
			System.out.println("Error while creating new document builder");
		}

          	for(int i=0; i<resourceCount; i++) { 
			topState.add(Constants.INITIAL_STATE_STR);
       		}

		// Used to keep track of visited states
		// and avoid infinite loops
		Set<String> visited = new HashSet<String>();

		SimulationTriple contState = 
				new SimulationTriple(
				topState, // topology state
				Constants.INITIAL_STATE_STR, // recipe state
				new ResourceVector());

		String v1 = contState.toString(resCount);

		visited.add(v1);
		operationsSchedule = traverseController(v1, visited, resCount);
		markRedundantNesting(operationsSchedule);
        }




        private Document traverseController(String v1, Set<String> visited, int resCount) {

		Document opReqORseq = null;
		try {
			// Create a new XML segment to store
			// a choice over one or more operations
			// requests, i.e. an OR node
			opReqORseq = dBuilder.newDocument(); 
			initOrBranch(opReqORseq);
		} 
		catch(Exception e) { 
			System.out.println("Couldn't create new XML document"); 
		}

                for (ControllerTransition contEdge : contEdges) {
			String src = contEdge.source.toString(resCount);

			// Look for outgoing edges from vertex 'v1'
                        if(src.equals(v1)) {

				// Create a new XML segment to store a
				// sequence of one or more (possibly atomic
				// choice points. Sequence are denoted by an 
				// 'AND' XML tag. If there are redundant AND 
				// tags they are removed later
                                Document opReqSEQwithANDtags = dBuilder.newDocument();
                                initAndBranch(opReqSEQwithANDtags);

				boolean alreadyVisitedv2 = Helper.isIn(contEdge.target.toString(), visited);
				String v2 = contEdge.target.toString(resCount); 
		
				// Each element in 'topLabSeq' is a list of
				// parallel operations (one per resource),
				// and so 'topLabSeq' contains the sequence
				// of topology transitions that were performed
				// between two given topology states
				List<List<Object>> topLabSeq = contEdge.topologyLabelSequence;	
				int resourceNum = 0;

                                for(List<Object> topLab : topLabSeq) {

					//int inResource = -1;
					//int outResource = -1;
					int inArr[] = initArray(resCount);
					int outArr[] = initArray(resCount); 

					resourceNum = 0;
					boolean transferFound = false;

					// Because of the way the controller was built,
					// we know that there must be matching 'in' and
					// 'out' synchronisations (or 'transfer operations').
					// For example, we *cannot* have a topology transition 
					// of the form [in:1, out:1, out:1] whereas we *can*
					// have one of the form [in:1, out:1, out:2, in:2].
                                	for(Object lab : topLab) {
						String labStr = (String)lab;
						if(labStr.startsWith(Constants.INSYNC)) {
							inArr[resourceNum] = Helper.number(labStr); 
							transferFound = true;
						}
						else if(labStr.startsWith(Constants.OUTSYNC)) {
							outArr[resourceNum] = Helper.number(labStr);
							transferFound = true;
						}
						else if(labStr.startsWith(Constants.NOP)) {
							// this resource must idle
						}
						else {
							// Create an operations request and assign it
							// to be executed by resourceNum + 1 (resource
							// numbers start from 0).
							CreateOpRequest opReq = new CreateOpRequest(
								Constants.B2MMLfolder+lab+Constants.B2MML_EXT,
								resourceNum+1);

							Document opReqInstantiated = opReq.getOpReq();

							if(opReqInstantiated != null) {
								// Append opReqInstantiated to opReqSEQwithANDtags  
								insertANDNode(opReqInstantiated, opReqSEQwithANDtags);
							}
						}
						resourceNum++;
					}

					// Create an operations request that corresponds 
					// to the parallel transfer of a parts
					if(transferFound) {
				        	createTransferOpReq(inArr, outArr, resCount, opReqSEQwithANDtags);
					}
				}

				Document result = dBuilder.newDocument();

				if(!alreadyVisitedv2) {
					visited.add(v2);		
					result = traverseController(v2, visited, resCount);
				}

				// Once the entire (individual) controller transition 
				// has been processed, append the next controller
				// transition's 'result' to the end of this particular
				// controller transitions sequence of operations
				// requests (opReqSEQwithANDtags)
				insertANDNode(result, opReqSEQwithANDtags);
			
				// As per the controller, this particular sequence 
				// (opReqSEQwithANDtags) becomes just one possible
				// option in a choice point of possibly more options.
				insertORNode(opReqSEQwithANDtags, opReqORseq);
                        }
                }       
		return opReqORseq;
        }




	/*
         * Convert the operations schedule (Document) into a string.
         */

	public String toString() {
    		try {
        		StringWriter sw = new StringWriter();
        		TransformerFactory tf = TransformerFactory.newInstance();
        		Transformer transformer = tf.newTransformer();
        		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
        		transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        		transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        		transformer.transform(new DOMSource(operationsSchedule), new StreamResult(sw));

        		return removeUselessTags(sw.toString());
    		} catch (Exception ex) {
        		throw new RuntimeException("Error converting to String", ex);
    		}
	}




	/*
 	 * Using <Sequence> and <ChoicePoint> tags are
 	 * useful when it comes to building the operations
 	 * schedule, but there might be redundant tags that 
 	 * can be removed once the operations schedule has 
 	 * been built. There are better ways to do this
 	 * though, so this function should be improved/replaced.
 	 */

	public String removeUselessTags(String b2mml) {

		String emptySeqTag = "<"+SEQUENCE_STR+"/>";
		String emptyChoiceTag = "<"+CHOICE_PT_STR+"/>";
		String emptySeqTag2 = "<"+SEQUENCE_STR+"></"+SEQUENCE_STR+">";
		String emptyChoiceTag2 = "<"+CHOICE_PT_STR+"></"+CHOICE_PT_STR+">";
		String emptyStr = "";

		// We use a temporary XML tag so that 
		// it can be temporarily inserted into
		// the document and then stripped out later
		// by an XML reader
		String redundant1 = "<"+REDUNDANT_STR+">";
		String redundant2 = "</"+REDUNDANT_STR+">";
		String redundant3 = "<"+REDUNDANT_STR+"/>";
		String startXML = Constants.startXML;

                b2mml = b2mml.substring(startXML.length());

		b2mml = b2mml.replaceAll(redundant1, emptyStr);
		b2mml = b2mml.replaceAll(redundant2, emptyStr);
		b2mml = b2mml.replaceAll(redundant3, emptyStr);
		b2mml = b2mml.replaceAll("\\>\\s+\\<","><");
		b2mml = b2mml.replaceAll(emptySeqTag, emptyStr);
		b2mml = b2mml.replaceAll(emptyChoiceTag, emptyStr);

		while(b2mml.indexOf(emptySeqTag2) >= 0 ||
		      b2mml.indexOf(emptyChoiceTag2) >= 0) {
			b2mml = b2mml.replaceAll(emptySeqTag2, emptyStr);
			b2mml = b2mml.replaceAll(emptyChoiceTag2, emptyStr);
		}

		// The current approach requires recreating the 
		// date that was inserted earlier during B2MML
		// initialisation. This is a bit hacky.
                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                Date date = new Date();

		b2mml = startXML +
			"<"+Constants.OP_SCHED_XML+"><"+Constants.PUB_DATE_XML+">" +
			dateFormat.format(date) +
			"</"+Constants.PUB_DATE_XML+">" + b2mml.trim() + 
			"</"+Constants.OP_SCHED_XML+">";

		return b2mml;
	}




	private void initOrBranch(Document d1) {
		Element rootElement = d1.createElement(CHOICE_PT_STR);
		d1.appendChild(rootElement);
	}




	private void initAndBranch(Document d1) {
		Element rootElement = d1.createElement(SEQUENCE_STR);
		d1.appendChild(rootElement);
	}




	private void initB2MML(Document d1) {

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();

		Element rootElement = d1.createElement(Constants.OP_SCHED_XML);
		Element dateElement = d1.createElement(Constants.PUB_DATE_XML);
		dateElement.appendChild(d1.createTextNode(dateFormat.format(date)));
		rootElement.appendChild(dateElement);
		d1.appendChild(rootElement);
	}




	private void markRedundantNesting(Document d) {
		Element root = d.getDocumentElement();
		markRedundantNesting(root, d);
	}




	/*
 	 * If there is no real need for a ChoicePoint 
 	 * or Sequence tag, i.e. the tag at most one 
 	 * child, then mark the tag for deletion by 
 	 * renaming it to REDUNDANT_STR. Do this 
 	 * recursively for each child and thereby for 
 	 * the entire operations schedule, starting 
 	 * from the root tag in the schedule.
 	 */

	private void markRedundantNesting(Node parent, Document d) {
		NodeList children = parent.getChildNodes();

		if(parent instanceof Element && 
		   ( ((Element)parent).getTagName().equals(CHOICE_PT_STR) ||
		     ((Element)parent).getTagName().equals(SEQUENCE_STR) ||
		     ((Element)parent).getTagName().equals(REDUNDANT_STR) 
  		   ) && children.getLength() <= 1) {

			d.renameNode(parent, parent.getNamespaceURI(), REDUNDANT_STR);
		}

		for(int i=0; i<children.getLength(); i++) {
			Node child = children.item(i);
			if(child instanceof Element &&
			   ((Element)child).getTagName().equals(SEQUENCE_STR) &&
			     ( ((Element)parent).getTagName().equals(SEQUENCE_STR) ||  
			       ((Element)parent).getTagName().equals(REDUNDANT_STR)
			     )) 
			{
				d.renameNode(child, child.getNamespaceURI(), REDUNDANT_STR);
			}
			markRedundantNesting(child, d);
		}
	}




	private void insertORNode(Document d1, Document d2) {
		Element andNode = d1.getDocumentElement();
		Element choice = d2.getDocumentElement();
		Node n = d2.importNode(andNode, true);
               	choice.appendChild(n);
	}




        private void insertANDNode(Document d1, Document d2) {
        	Element opReqt_or_choice = d1.getDocumentElement();
                Element seq = d2.getDocumentElement();
                Node n = d2.importNode(opReqt_or_choice, true);
                seq.appendChild(n);
        }




        private int [] initArray(int size) {

		int [] arr = new int[size];

		for(int i=0; i<size; i++) 
			arr[i] = EMPTY;

		return arr;
	}




	private void createTransferOpReq(int [] inArr, int [] outArr, int resCount, Document opReqSEQwithANDtags) {

        	for(int inRes=0; inRes < resCount; inRes++) {
                	for(int outRes=0; outRes < resCount; outRes++) {
                        	if(inArr[inRes] == outArr[outRes] && inArr[inRes] != EMPTY) {
                                                 
                                	CreateOpRequest opReq = new CreateOpRequest(
                                        	Constants.B2MMLfolder+Constants.TRANSFER_B2MML, 
						inRes+1, outRes+1);

                                   	Document opReqInstantiated = opReq.getOpReq();

                                       	if(opReqInstantiated != null) {
                                     		insertANDNode(opReqInstantiated, opReqSEQwithANDtags); 
                                   	}

					break;
                		}
            		}
		}
	}
}
