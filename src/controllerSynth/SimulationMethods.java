package controllerSynth;
import java.awt.*;
import java.io.*;
import java.util.*;
import java.util.stream.*;
import java.util.List;
import java.nio.file.*;
import java.nio.charset.*;

/**
 * This class implements the algorithms presented in the paper:
 * 	Realisability of Production Recipes, by
 * 	de Silva, Felli, Chaplin, Logan, Sanderson, Ratchev, 
 * 	published in the European Conference on Artificial Intelligence, 2016.
 */

public class SimulationMethods {

    private Set < RecipeTransition > recipeEdges = new HashSet < RecipeTransition > ();
    private Set < TopologyTransition > topologyEdges = new HashSet < TopologyTransition > ();
    private boolean debug = false;
    private int resourceCount = 0;




    public SimulationMethods(Set < RecipeTransition > recipeTrans, Set < TopologyTransition > topologyTrans, int resCnt, boolean debug) {

        this.recipeEdges = recipeTrans;
        this.topologyEdges = topologyTrans;
        this.debug = debug;
        this.resourceCount = resCnt;
    }




    public Object[] findSim(Sigma sigma, String recipeState) {

        Object[] result = {
            new HashSet < SimulationTriple > (),
            new HashSet < ControllerTransition > ()
        };


        Set < SimulationTriple > sim = new HashSet < SimulationTriple > ();
        Set < ControllerTransition > cont = new HashSet < ControllerTransition > ();

        for (RecipeTransition recipeEdge: recipeEdges) {
            if (recipeEdge.source.equals(recipeState)) {
                List < List < Object >> labSeq = new ArrayList < List < Object >> ();

                OperationExpression newLabel = new OperationExpression(recipeEdge.label);

                result = evalExp(sigma, newLabel, recipeEdge, labSeq, sigma, new HashSet < Sigma > ());
                if (((Set) result[Constants.SIMU_RELATION]).isEmpty()) {
                    result[Constants.SIMU_RELATION] = new HashSet < SimulationTriple > ();
                    result[Constants.CONTROLLER] = new HashSet < ControllerTransition > ();
                    return result;
                }
                sim.addAll((Set) result[Constants.SIMU_RELATION]);
                cont.addAll((Set) result[Constants.CONTROLLER]);
            }
        }

        if (debug) {
            System.out.println("Resource Vector in FindSim");
            sigma.resourceVector.printAsString(resourceCount);
        }
        SimulationTriple curr = new SimulationTriple(sigma.topologyState, recipeState, sigma.resourceVector);
        sim.add(curr);
        result[Constants.SIMU_RELATION] = sim;
        result[Constants.CONTROLLER] = cont;

        return result;
    }




    public Object[] evalExp(Sigma sigmaNxt,
        OperationExpression tCur,
        RecipeTransition tr,
        List < List < Object >> labSeq,
        Sigma sigma0,
        Set < Sigma > visited) {

        if (debug)
            System.out.println("\nInitial task expression (tCUR): " + tCur.label);
        if (tCur.isEmpty()) {
            if (debug)
                System.out.println("Current task expression (tCUR) is empty");
            SimulationTriple stSource = new SimulationTriple(sigma0.topologyState, tr.source, sigma0.resourceVector);
            SimulationTriple stTarget = new SimulationTriple(sigmaNxt.topologyState, tr.target, sigmaNxt.resourceVector);

            ControllerTransition ct = new ControllerTransition(stSource, tr.label, labSeq, stTarget);
            Object[] result = findSim(sigmaNxt, tr.target);
            ((Set) result[Constants.CONTROLLER]).add(ct);

            return result;
        }

        String firstExpr = tCur.nextExpr();

        for (TopologyTransition topologyEdge: topologyEdges) {

            // TEMPORARY solution until List<Object> 
            // is replaced with List<String> in all classes
            List <String> source1 = topologyEdge.source.stream()
                .map(object -> Objects.toString(object, null))
                .collect(Collectors.toList());

            List <String> source2 = sigmaNxt.topologyState.stream()
                .map(object -> Objects.toString(object, null))
                .collect(Collectors.toList());     	
        	
        	

            if (equals(source1, source2)) {

                Object[] result = {
                    new HashSet < SimulationTriple > (),
                    new HashSet < ControllerTransition > ()
                };


                if (!Helper.contains(visited, sigmaNxt, resourceCount)) {
                    if (debug)
                        System.out.println("Trying to allocate " + firstExpr);
                    ResourceVector rv = allocate(sigmaNxt.resourceVector, firstExpr, topologyEdge.label);
                    List < List < Object >> labSeqNew = null;

                    if (rv != null) {
                        if (debug) {
                            System.out.println("Allocation successful");
                            System.out.println("Resource's parts before move:");
                        }
                        if (debug) {
                            rv.printAsString(resourceCount);
                        }

                        rv = move(rv, topologyEdge.label);

                        // NULL is returned if an 'out' label has no parts on it (i.e.
                        // at its corresponding index in the resource vector)
                        if (rv != null) {
                            if (debug) {
                                System.out.println("Resource's parts after move:");
                                rv.printAsString(resourceCount);
                            }
                            labSeqNew = Helper.clone(labSeq);
                            labSeqNew.add(topologyEdge.label);
                            Sigma sigmaPrime = new Sigma(topologyEdge.target, rv);
                            OperationExpression tCurNew = new OperationExpression(tCur);

                            result = evalExp(sigmaPrime,
                                tCurNew,
                                tr,
                                labSeqNew,
                                sigma0,
                                new HashSet < Sigma > ());
                        }
                    }

                    if (((Set) result[Constants.SIMU_RELATION]).isEmpty() &&
                        isUnobservable(topologyEdge.label)) {
                        if (debug)
                            System.out.println("Topology transition is unobservable");

                        rv = move(sigmaNxt.resourceVector, topologyEdge.label);

                        if (rv != null) {
                            Sigma sigmaPrime = new Sigma(topologyEdge.target, rv);

                            labSeqNew = Helper.clone(labSeq);
                            labSeqNew.add(topologyEdge.label);
                            Set < Sigma > newVisited = Helper.clone(visited);
                            newVisited.add(sigmaNxt);

                            OperationExpression tCurNew = new OperationExpression(tCur);
                            tCurNew.undoOneStep();

                            result = evalExp(sigmaPrime,
                                tCurNew,
                                tr,
                                labSeqNew,
                                sigma0,
                                newVisited);
                        }
                    }

                    if (!((Set) result[Constants.SIMU_RELATION]).isEmpty()) {
                        return result;
                    }
                } else {
                    if (debug) {
                        System.out.println("Already visited the following state/rv");
                        System.out.println(sigmaNxt.topologyState);
                        sigmaNxt.resourceVector.printAsString(resourceCount);
                    }
                }
            }
        }

        Object[] newResult = {
            new HashSet < SimulationTriple > (),
            new HashSet < ControllerTransition > ()
        };
        return newResult;
    }




    private ResourceVector allocate(ResourceVector rVect, String atomicOpExpr, List < Object > topologyLabel) {

        int resCnt = 0;
        int allocated = 0;
        ResourceVector rVectNew = new ResourceVector(rVect);

        List < Object > atomicExprList = Helper.getAtomicExprAsList(atomicOpExpr);

        for (Object lab: topologyLabel) {

            if (debug) {
                System.out.println("\nP-task:" + name(atomicOpExpr) + "; task:" + lab.toString() + ".");
                System.out.println("\nParts in resource:" + rVectNew.rv[resCnt]);
                System.out.println("P-task's input parts:" + inputParts(atomicOpExpr) +
                    "\nP-task's output parts:" + outputParts(atomicOpExpr));
            }

            if (lab.toString().equals(Constants.NOP) ||
                lab.toString().startsWith(Constants.INSYNC) ||
                lab.toString().startsWith(Constants.OUTSYNC)) {

            } else {
                boolean found = false;
                for (Object labExpr: atomicExprList) {
                    if (lab.toString().equals(name((String) labExpr)) &&
                        equals(rVectNew.rv[resCnt], inputParts((String) labExpr))) {
                        allocated++;
                        rVectNew.rv[resCnt] = outputParts((String) labExpr);
                        found = true;
                    }
                }

                if (!found) {
                    return null;
                }
            }

            resCnt++;
        }

        if (atomicExprList.size() != allocated) {
            return null;
        } else {
            return rVectNew;
        }
    }




    private boolean equals(List < String > a, List < String > b) {

        int i = 0;

        if (a.size() != b.size())
            return false;

        for (String str: a) {
            if (!str.equals(b.get(i).toString())) {
                return false;
            }
            i++;
        }

        return true;
    }




    private String name(String atomicOpExpr) {

        StringTokenizer toks = new StringTokenizer(atomicOpExpr, "()");
        return toks.nextToken();
    }




    private List < String > inputParts(String atomicOpExpr) {

        List < String > inputParts = new ArrayList < String > ();
        StringTokenizer toks = new StringTokenizer(atomicOpExpr, Constants.PARAMDELIMITERS);
        toks.nextToken(); // remove the operation name

        // input parts are empty 
        if (toks.countTokens() < Constants.NUM_OF_PARAM_CLASSES) {
            return inputParts;
        }
        if (toks.hasMoreTokens()) {
            StringTokenizer toksInput = new StringTokenizer(toks.nextToken(), ", ");

            while (toksInput.hasMoreTokens()) {
                inputParts.add(toksInput.nextToken());
            }
        }
        return inputParts;
    }




    private List < String > outputParts(String atomicOpExpr) {

        List < String > outputParts = new ArrayList < String > ();
        StringTokenizer toks = new StringTokenizer(atomicOpExpr, Constants.PARAMDELIMITERS);
        toks.nextToken(); // remove the name
        if (toks.hasMoreTokens()) {
            if (toks.countTokens() == Constants.NUM_OF_PARAM_CLASSES) {
                toks.nextToken(); // remove the input parts 
            }
            if (toks.hasMoreTokens()) {
                StringTokenizer toksOutput = new StringTokenizer(toks.nextToken(), ", ");
                while (toksOutput.hasMoreTokens()) {
                    outputParts.add(toksOutput.nextToken());
                }
            }
        }
        return outputParts;
    }



    /**
     * Performs a move of parts from the 'input' resource vector to the 'output' resource
     * vector, based on the 'out:x' and corresponding 'in:x' transitions on a given topology
     * transition label. However, if there is an 'out:x' synchronisation being attempted with 
     * no parts at that particular index, then NULL is returned.
     */

    private ResourceVector move(ResourceVector rVect, List < Object > topologyLabel) {

        int inIndex, outIndex = 0;
        ResourceVector rVectNew = new ResourceVector(rVect);

        for (Object labOut: topologyLabel) {
            if (labOut.toString().startsWith(Constants.OUTSYNC)) {
                inIndex = 0;
                for (Object labIn: topologyLabel) {
                    if (labIn.toString().startsWith(Constants.INSYNC) &&
                        Helper.number(labOut) == Helper.number(labIn)) {
                        if (!rVectNew.rv[outIndex].isEmpty()) {
                            if (debug) {
                                System.out.println("\nResource vector before (in): " + rVectNew.rv[inIndex]);
                                System.out.println("Resource vector before move (out): " + rVectNew.rv[outIndex]);
                            }
                            rVectNew.rv[inIndex].add((String) rVectNew.rv[outIndex].remove(0));
                            if (debug) {
                                System.out.println("Resource vector after move (in): " + rVectNew.rv[inIndex]);
                                System.out.println("Resource vector after move (out): " + rVectNew.rv[outIndex]);
                            }
                            break;
                        } else {
                            if (debug)
                                System.out.println("\nReturning NULL: there is an 'out:" + outIndex + "' but with no part (in the resource vector) at the same index.");
                            return null;
                        }
                    }
                    inIndex++;
                }
            }
            outIndex++;
        }

        return rVectNew;
    }




    private boolean isUnobservable(List < Object > topologyLabel) {

        for (Object lab: topologyLabel) {
            System.out.print(lab.toString() + " is ");
            if (!lab.toString().startsWith(Constants.UNOBSERVABLE_PREFIX) &&
                !lab.toString().equals(Constants.NOP) &&
                !lab.toString().startsWith(Constants.INSYNC) &&
                !lab.toString().startsWith(Constants.OUTSYNC)) {
                System.out.println("observable.");
                return false;
            }
            System.out.println("unobservable.");
        }

        return true;
    }




    private boolean isParallelExpr(String taskExpr) {

        return (taskExpr.toString().indexOf(Constants.PARALLEL_SYMBOL) != -1);
    }
}