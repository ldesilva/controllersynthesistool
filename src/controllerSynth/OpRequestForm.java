package controllerSynth;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import java.io.*;
import java.nio.file.*;
import java.nio.charset.*;
import java.util.*;
import javax.swing.*;
import java.awt.event.WindowEvent;
import javax.swing.event.*;




public class OpRequestForm extends JPanel {

   public static OpRequestForm newF;

   public int MAX_FIELDS = 200;
   private String[] opTypeStrings = {"Production", "Loading", "Routing"};
   public JComboBox nameField1 = new JComboBox(opTypeStrings);
   public ArrayList<JTextField> nameFields;
   public ArrayList<JComboBox> nameFieldsCombo;
   public ArrayList<JComboBox> nameFieldsComboMatReq = new ArrayList<JComboBox>(MAX_FIELDS);
   public ArrayList<JTextField> nameFieldsSegParam = new ArrayList<JTextField>(MAX_FIELDS); 
   public ArrayList<JTextField> nameFieldsMatReq = new ArrayList<JTextField>(MAX_FIELDS); 

   public int nameFieldCnt = -1;
   public int nameFieldCnt2 = -1;
   private int LENGTH = 5;

   public JDialog frame; 
   private String[] partStrings = {"Consumed", "Produced"};

   private JComboBox searchTermsCombo = new JComboBox();
   private JButton addNewFieldBtn = new JButton("Add Mat. Req.");
   private JButton addNewParamBtn = new JButton("Add Seg. Param.");
   private JButton saveFieldBtn = new JButton("Save");
   private JButton submitBtn = new JButton("Next Operations Request");
   private JPanel centerPanel = new JPanel(new GridBagLayout());
   private int gridY = 0;
   private static String filename = "";  
   private String pad = "     "; 
   private String padplus = "  +  "; 
   private String AUTO_INSERTED = " - to be auto-filled later -"; 
   public boolean next = false;
   public boolean cancel = false;
   public int numOfMatReq = 0;
   public int numOfSegParam = 0;
   private JDialog frame2;
   public GridBagConstraints gbc;




   public OpRequestForm() {
   
      nameFields = new ArrayList<JTextField>(MAX_FIELDS); 
      nameFieldsCombo = new ArrayList<JComboBox>(MAX_FIELDS); 

      for(int i=0; i< MAX_FIELDS; i++) {
         nameFields.add(new JTextField(LENGTH)); 
   	 nameFieldsCombo.add(new JComboBox(partStrings));

      	 nameFields.get(i).getDocument().addDocumentListener(new DocumentListener() {
  	    public void changedUpdate(DocumentEvent e) {
	        saveFieldBtn.setEnabled(true);
	    }
  	    public void removeUpdate(DocumentEvent e) {
	        saveFieldBtn.setEnabled(true);
	    }
  	    public void insertUpdate(DocumentEvent e) {
	        saveFieldBtn.setEnabled(true);
	    }
         });

      	 nameFieldsCombo.get(i).addActionListener(new ActionListener() {
  	    public void actionPerformed(ActionEvent e) {
	        saveFieldBtn.setEnabled(true);
	    }
         });
      }
   }




   public void setup(JDialog frametmp, String opName) {
      this.frame2 = frametmp;
      gbc = createGBC(0, gridY);
      centerPanel.add(new JLabel(Constants.OP_TYP_XML+":"), gbc);
      gbc = createGBC(2, gridY);
      centerPanel.add(nameField1, gbc);     
      gridY++;

      add("ID:", null, true, true);
      add("Description:", null, true, true);

      gbc = createGBC(0, gridY); 
      centerPanel.add(new JLabel(), gbc); 
      gridY++;

      gbc = createGBC(0, gridY);
      JLabel txt1 = new JLabel("SegmentRequirement");
      txt1.setForeground(Color.GRAY);
      centerPanel.add(txt1, gbc);
      gridY++;

      add(padplus+"ID:", null, false, true);
      add(padplus+"Description:", null, true, true);

      // Add the fields for all segment parameters 
      // to the current panel
      for(int i=0; i < numOfSegParam; i++) {
         addNewSegParam(null);
      }

      gbc = createGBC(0, gridY); centerPanel.add(new JLabel(), gbc); gridY++;

      gbc = createGBC(0, gridY);
      JLabel txt2 = new JLabel(padplus+"MaterialRequirement");
      txt2.setForeground(Color.GRAY);
      centerPanel.add(txt2, gbc);
      gridY++;

      add(pad+padplus+"MaterialUse:", null, true, false);
      nameFieldsComboMatReq.add(nameFieldsCombo.get(nameFieldCnt));
      add(pad+padplus+"Quantity:", null, true, true);
      nameFieldsMatReq.add(nameFields.get(nameFieldCnt));
      gbc = createGBC(0, gridY); centerPanel.add(new JLabel(), gbc); gridY++;

      gbc = createGBC(0, gridY);
      JLabel txt3 = new JLabel(pad+padplus+"MaterialRequirementProperty");
      txt3.setForeground(Color.GRAY);
      centerPanel.add(txt3, gbc);
      gridY++;
      
      add(pad+pad+padplus+"ID:", null, false, true);
      nameFieldsMatReq.add(nameFields.get(nameFieldCnt));
      add(pad+pad+padplus+"Value:", null, true, true);
      nameFieldsMatReq.add(nameFields.get(nameFieldCnt));

      gbc = createGBC(0, gridY); centerPanel.add(new JLabel(), gbc); gridY++;

      // Add the fields for all remaining
      // materials requirements
      for(int i=1; i < numOfMatReq; i++) {
         addNewMatReq(null);
      } 

      gbc = createGBC(0, gridY); centerPanel.add(new JLabel(), gbc); gridY++;

      addNewParamBtn.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) {
	    newF.numOfSegParam++;
            addNewSegParam(e);
         }
      });

      addNewFieldBtn.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) {
	    newF.numOfMatReq++;
            addNewMatReq(e);
         }
      });

      saveFieldBtn.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) {
		saveOpReq();
                saveFieldBtn.setEnabled(false);
         }
      });

      JPanel bottomPanel = new JPanel();
      bottomPanel.setLayout(new BoxLayout(bottomPanel, BoxLayout.PAGE_AXIS));
      JPanel addNewFieldPanel = new JPanel(new GridLayout(1, 0));
      addNewFieldPanel.add(addNewParamBtn);
      addNewFieldPanel.add(addNewFieldBtn);
      addNewFieldPanel.add(saveFieldBtn);
      JPanel submitPanel = new JPanel(new BorderLayout());
      bottomPanel.add(addNewFieldPanel);
      bottomPanel.add(Box.createVerticalStrut(5));
      bottomPanel.add(submitPanel);

      int eb = 8;
      setBorder(BorderFactory.createEmptyBorder(eb, eb, eb, eb));
      setLayout(new BorderLayout());
      add(centerPanel, BorderLayout.CENTER);
      add(bottomPanel, BorderLayout.SOUTH);
   }




   /*
    * Create the three (empty) text fields that correspond
    * to a new segment parameter. The values for these fields 
    * are filled in later.
    */

   public void addNewSegParam(ActionEvent e) {

      JLabel border = new JLabel();
      GridBagConstraints gbc = createGBC(0, gridY); centerPanel.add(border, gbc); gridY++;
      gbc = createGBC(0, gridY);
      JLabel txt6 = new JLabel(padplus+"SegmentParameter");
      txt6.setForeground(Color.GRAY);
      centerPanel.add(txt6, gbc);
      gridY++;

      add(pad+padplus+"ID:", null, true, true);
      nameFieldsSegParam.add(nameFields.get(nameFieldCnt));
      add(pad+padplus+"ValueString:", null, true, true);
      nameFieldsSegParam.add(nameFields.get(nameFieldCnt));
      add(pad+padplus+"Description:", null, true, true);
      nameFieldsSegParam.add(nameFields.get(nameFieldCnt));

      gbc = createGBC(0, gridY); centerPanel.add(new JLabel(), gbc); gridY++;

      Window win = SwingUtilities.getWindowAncestor(addNewParamBtn);
      if (win != null) {
         win.pack();
         win.setLocationRelativeTo(null);
      }
   }




   private void addNewMatReq(ActionEvent e) {

      GridBagConstraints gbc = createGBC(0, gridY); centerPanel.add(new JLabel(), gbc); gridY++;

      gbc = createGBC(0, gridY);
      JLabel txt4 = new JLabel(padplus+"MaterialRequirement");
      txt4.setForeground(Color.GRAY);
      centerPanel.add(txt4, gbc);
      gridY++;

      add(pad+padplus+"MaterialUse:", null, true, false);
      nameFieldsComboMatReq.add(nameFieldsCombo.get(nameFieldCnt));
      add(pad+padplus+"Quantity:", null, true, true);
      nameFieldsMatReq.add(nameFields.get(nameFieldCnt));

      gbc = createGBC(0, gridY); centerPanel.add(new JLabel(), gbc); gridY++;

      gbc = createGBC(0, gridY);
      JLabel txt5 = new JLabel(pad+padplus+"MaterialRequirementProperty");
      txt5.setForeground(Color.GRAY);
      centerPanel.add(txt5, gbc);
      gridY++;

      add(pad+pad+padplus+"ID:", null, false, true);
      nameFieldsMatReq.add(nameFields.get(nameFieldCnt));
      nameFields.get(nameFieldCnt).setText(AUTO_INSERTED);
      nameFields.get(nameFieldCnt).setEnabled(false);

      add(pad+pad+padplus+"Value:", null, false, true);
      nameFieldsMatReq.add(nameFields.get(nameFieldCnt));

      gbc = createGBC(0, gridY); centerPanel.add(new JLabel(), gbc); gridY++;

      Window win = SwingUtilities.getWindowAncestor(addNewFieldBtn);
      if (win != null) {
         win.pack();
         win.setLocationRelativeTo(null);
      }
   }




   private GridBagConstraints createGBC(int x, int y) {
      GridBagConstraints gbc = new GridBagConstraints();
      gbc.gridx = x;
      gbc.gridy = y;
      gbc.gridwidth = 1;
      gbc.gridheight = 1;
      gbc.weightx = 1.0;
      gbc.weighty = 1.0;
      gbc.anchor = (x == 0) ? gbc.LINE_START : gbc.LINE_END;
      gbc.fill = (x == 0) ? gbc.BOTH : gbc.HORIZONTAL;
      gbc.insets = (x == 0) ? new Insets(5, 0, 5, 5) : new Insets(5, 5, 5, 0);
      return gbc;
   }




   public boolean createAndShowGui(JFrame parent, String edgeSym) {

	int id=0;
	int val=0;
	int desc=0;

	newF = new OpRequestForm();
	//newF.setPreferredSize(new Dimension(700, 1000));	

	try {
   		DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
        	domFactory.setIgnoringComments(false);
        	DocumentBuilder builder = domFactory.newDocumentBuilder();
		filename = Constants.B2MMLfolder+edgeSym+".xml";
        	Document operationsRequest;

		try{
        		operationsRequest = builder.parse(new File(filename));
		}
		catch(Exception ee) {
			String filenameTmp = Constants.B2MMLfolder+Constants.TEMPLATE_FILE;
        		operationsRequest = builder.parse(new File(filenameTmp));
		}

		// The first few fields of an operations-request
		NodeList nodes = operationsRequest.getElementsByTagName("OperationsType");
		newF.nameField1.setSelectedItem(nodes.item(0).getTextContent());
		newF.nameFields.get(++nameFieldCnt2).setText(edgeSym);
		id++;

		newF.nameFields.get(nameFieldCnt2).setEnabled(false);
		
		nodes = operationsRequest.getElementsByTagName("Description");
		newF.nameFields.get(++nameFieldCnt2).setText(nodes.item(desc++).getTextContent());

		// The first two fields of the operations-request's segment requirement 
		// (assuming that there's only one segment requirement per operation-request)
		
		// The ID will be filled in later. Since the B2MML file 
		// will not have an ID tag at all at this point in the file, 
		// we do not increment the 'id' variable.
		newF.nameFields.get(++nameFieldCnt2).setText(AUTO_INSERTED);
		newF.nameFields.get(nameFieldCnt2).setEnabled(false);
		// Description
		desc++;
		newF.nameFields.get(++nameFieldCnt2).setText(AUTO_INSERTED);
		newF.nameFields.get(nameFieldCnt2).setEnabled(false);

		// Search for a segment parameter, which must have a 'ValueString' 
		// attribute (there may not be any segment parameters at all)
		NodeList nodesSegParam = operationsRequest.getElementsByTagName("ValueString");

		for(int i=0; i < nodesSegParam.getLength(); i++) {
			nodes = operationsRequest.getElementsByTagName("ID");
			newF.nameFields.get(++nameFieldCnt2).setText(nodes.item(id++).getTextContent());
			nodes = operationsRequest.getElementsByTagName("ValueString");
			newF.nameFields.get(++nameFieldCnt2).setText(nodes.item(i).getTextContent());
			nodes = operationsRequest.getElementsByTagName("Description");
			newF.nameFields.get(++nameFieldCnt2).setText(nodes.item(desc++).getTextContent());
		}

		newF.numOfSegParam = nodesSegParam.getLength();
		
		int loopCnt = 0;

		nodes = operationsRequest.getElementsByTagName("MaterialRequirement");

		// Search for a material requirement. There will be at least one:
		// even if there's no input, there must be at least one output. 
		do {
			newF.nameFieldsCombo.get(++nameFieldCnt2).setSelectedItem(operationsRequest.getElementsByTagName("MaterialUse").item(loopCnt).getTextContent());
			newF.nameFields.get(++nameFieldCnt2).setText(operationsRequest.getElementsByTagName("Quantity").item(loopCnt).getTextContent());
			id++;
			newF.nameFields.get(++nameFieldCnt2).setText(AUTO_INSERTED);
			newF.nameFields.get(nameFieldCnt2).setEnabled(false);
			newF.nameFields.get(++nameFieldCnt2).setText(operationsRequest.getElementsByTagName("Value").item(loopCnt).getTextContent());
			loopCnt++;

		} while(loopCnt < nodes.getLength());

		newF.numOfMatReq = nodes.getLength();
	}
	catch(Exception e) {
		System.out.println("ERROR: field missing in XML?");
	}

	newF.setup(frame, edgeSym);
        newF.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createTitledBorder(
                        BorderFactory.createLoweredBevelBorder(),
                        "Create / Edit Operations Template",
                        javax.swing.border.TitledBorder.CENTER,
                        javax.swing.border.TitledBorder.ABOVE_TOP),
                        BorderFactory.createEmptyBorder(15,15,15,15)));

        JScrollPane scrollPane = new JScrollPane(newF);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setPreferredSize(new Dimension(800, 500));

      	frame = new JDialog(); // Create/Edit Operations Request
      	frame.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        frame.setModal(true);
      	frame.getContentPane().add(scrollPane);
      	frame.pack();
      	frame.setLocationRelativeTo(parent);
      	frame.setVisible(true);

	return newF.next;
   }




   public void show(JFrame parent, String edge) {
      SwingUtilities.invokeLater(new Runnable() {
         public void run() {
            createAndShowGui(parent, edge);
         }
      });
   }




   public void add(String label, Color color, boolean setEnabled, boolean textField) {

      gbc = createGBC(0, gridY);
      JLabel txt = new JLabel(label);
      if(color != null) 
         txt.setForeground(color);
      centerPanel.add(txt, gbc);
      gbc = createGBC(2, gridY);
      if(textField)
      	centerPanel.add(nameFields.get(++nameFieldCnt), gbc);
      else
      	centerPanel.add(nameFieldsCombo.get(++nameFieldCnt), gbc);
      gridY++;
   }




   public void saveOpReq() {

      String b2mml = "";
      String newline = "\n";

      b2mml += Constants.startXML + newline;

      b2mml += makeStart(Constants.OP_REQ_XML);

      b2mml += newline; 

      b2mml += makeStart(Constants.OP_TYP_XML);
      b2mml += newF.nameField1.getSelectedItem().toString();
      b2mml += makeEnd(Constants.OP_TYP_XML);

      b2mml += newline; 

      int cnt=0;

      b2mml += makeStart(Constants.OP_ID_XML);
      b2mml += newF.nameFields.get(cnt++).getText();
      b2mml += makeEnd(Constants.OP_ID_XML);

      b2mml += newline; 

      b2mml += makeStart(Constants.OP_DESC_XML);
      b2mml += newF.nameFields.get(cnt++).getText();
      b2mml += makeEnd(Constants.OP_DESC_XML);
      
      b2mml += newline; 

      b2mml += makeStart(Constants.OP_SEGREQ_XML);

      // Ignore the 'ID' field; this is a 
      // unique ID for the operations request,
      // which will be filled in automatically

      b2mml += makeStart(Constants.OP_DESC_XML);
      cnt++;
      b2mml += makeEnd(Constants.OP_DESC_XML);
     
      int segcnt = 0;
 
      for(int i=0; i<newF.numOfSegParam; i++) {

         b2mml += newline; 

         b2mml += makeStart(Constants.OP_SEGPARAM_XML);

         b2mml += makeStart(Constants.OP_ID_XML);
         b2mml += newF.nameFieldsSegParam.get(segcnt++).getText();
         b2mml += makeEnd(Constants.OP_ID_XML);

         b2mml += newline; 

         b2mml += makeStart(Constants.OP_VALSTR_XML);
         b2mml += newF.nameFieldsSegParam.get(segcnt++).getText();
         b2mml += makeEnd(Constants.OP_VALSTR_XML);

         b2mml += newline; 

         b2mml += makeStart(Constants.OP_DESC_XML);
         b2mml += newF.nameFieldsSegParam.get(segcnt++).getText();
         b2mml += makeEnd(Constants.OP_DESC_XML);

         b2mml += makeEnd(Constants.OP_SEGPARAM_XML);
      } 
     
      int matreqcnt = 0;
      int matreqCombocnt = 0;

      for(int i=0; i<newF.numOfMatReq; i++) {

         b2mml += newline; 

         b2mml += makeStart(Constants.OP_MATREQ_XML);

         b2mml += makeStart(Constants.OP_MATUSE_XML);

	 b2mml += newF.nameFieldsComboMatReq.get(matreqCombocnt++).getSelectedItem();
         b2mml += makeEnd(Constants.OP_MATUSE_XML);

         b2mml += makeStart(Constants.OP_QTY_XML);
         b2mml += newF.nameFieldsMatReq.get(matreqcnt++).getText();
         b2mml += makeEnd(Constants.OP_QTY_XML);

         b2mml += makeStart(Constants.OP_MATREQP_XML);

         b2mml += makeStart(Constants.OP_ID_XML);
         matreqcnt++;
         b2mml += makeEnd(Constants.OP_ID_XML);

         b2mml += makeStart(Constants.OP_VAL_XML);
         b2mml += newF.nameFieldsMatReq.get(matreqcnt++).getText();
         b2mml += makeEnd(Constants.OP_VAL_XML);
 
         b2mml += makeEnd(Constants.OP_MATREQP_XML);

         b2mml += makeEnd(Constants.OP_MATREQ_XML);
      }
 
      b2mml += makeEnd(Constants.OP_SEGREQ_XML);

      b2mml += makeEnd(Constants.OP_REQ_XML);

      try {
         Path file = Paths.get(filename);
         java.util.List<String> lines = new ArrayList<String>();
         lines.add(b2mml);
         Files.write(file, lines, Charset.forName("UTF-8"));
      }
      catch(Exception e) {
         System.out.println("Error writing operations-request to file");
      }
   }




   private String makeStart(String str) {
      return "<"+str.trim()+">";
   }



   private String makeEnd(String str) {
      return "</"+str.trim()+">";
   }
}
