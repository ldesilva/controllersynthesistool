package controllerSynth;
import java.util.*;

public class ResourceVector {

        public List<String> [] rv = new List[Constants.MAX_RESOURCES];

        public ResourceVector() {
		for(int i=0; i<Constants.MAX_RESOURCES; i++) {
			rv[i] = new ArrayList<String>();
		}
	}

        ResourceVector(ResourceVector rVect) {
		//super();	
	
		for(int i=0; i<Constants.MAX_RESOURCES; i++) {
			rv[i] = new ArrayList<String>();
			for(String str : rVect.rv[i]) {
				rv[i].add(new String(str));
			}
		}
	}


	//@Override
        public void printAsString(int resCount) {
		for(int i=0; i<resCount; i++) {
			System.out.println(rv[i]);
		}
	}


        public String getAsString(int resCount) {

		String rvNew = new String();

                for(int i=0; i<resCount; i++) {
                        rvNew += " "+rv[i].toString();
                }
		
		return rvNew;
        }

        //ResourceVector(List<String> [] rv) {
                //this.rv = rv;
        //}

}

