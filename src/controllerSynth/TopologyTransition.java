package controllerSynth;
import java.util.*;

public class TopologyTransition {

	public List<Object> source;
        public List<Object> label;
        public List<Object> target;

        public TopologyTransition(List<Object> a, List<Object> b, List<Object> c) {
        	this.source = a;
                this.label = b;
                this.target = c;
        }
	
	@Override
	public String toString() { 
    		return source + "====" + label + "====>" + target + "\n";
	} 
}

