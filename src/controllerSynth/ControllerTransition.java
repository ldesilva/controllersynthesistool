package controllerSynth;
import java.util.*;

public class ControllerTransition {

	public SimulationTriple source;
        public OperationExpression opExpr;
	public List<List<Object>> topologyLabelSequence;
        public SimulationTriple target;

        ControllerTransition(SimulationTriple a, OperationExpression b,  List<List<Object>> c, SimulationTriple d) {
        	this.source = a;
                this.opExpr = b;
                this.topologyLabelSequence = c;
                this.target = d;
        }
	
	//@Override
	public String toString(int resCnt) { 
    		return source.topologyState + " ==== " + opExpr.label + " ||| " + topologyLabelSequence + " ====> " + target.toString(resCnt) + "\n";
	} 
}

