package controllerSynth;
import java.awt.*;
import java.io.*;
import java.util.*;
import java.util.stream.*;
import java.util.List;
import java.nio.file.*;
import java.nio.charset.*;

public class TopologyMethods {

        private Set<List<Object>> topologyVertices = new HashSet<List<Object>>();
        private Set<TopologyTransition> topologyEdges = new HashSet<TopologyTransition>();
        private Object [] transOfAllResources = new Object[Constants.MAX_RESOURCES];

	private int resourceCount = 0;

	/**
 	 * This function is no longer used, but kept for reference. It computes 
 	 * the cartesian product between any two lists of lists of objects/strings.
 	 **/

	public TopologyMethods(int resCnt, Object [] transOfAllRes) {

		this.resourceCount = resCnt;
		this.transOfAllResources = transOfAllRes;
	}



 
	public <Object> List<List<Object>> cartesianProduct(List<List<Object>> lists) {
    		List<List<Object>> resultLists = new ArrayList<List<Object>>();
    		if (lists.size() == 0 || lists.get(0) == null) {
        		resultLists.add(new ArrayList<Object>());
        		return resultLists;
    		} else {
        		List<Object> firstList = lists.get(0);
			if(firstList != null) {
        			List<List<Object>> remainingLists = cartesianProduct(lists.subList(1, lists.size()));
        			for (Object condition : firstList) {
            				for (List<Object> remainingList : remainingLists) {
                				ArrayList<Object> resultList = new ArrayList<Object>();
                				resultList.add(condition);
                				resultList.addAll(remainingList);
                				resultLists.add(resultList);
            				}
				}
        		}
    		}
    		return resultLists;
	}




	/**
 	 * Compute the topology by starting from the initial
 	 * state of each resource, and then nondeterministically
 	 * picking a transition for each of the initial states 
 	 * to create a resulting state for each resource. 
 	 * The same is repeated recursively for the set of 
 	 * resulting states. Whenever a set of transitions is
 	 * picked from each resource, we need to ensure they
 	 * are consistent/valid: e.g., [out:1, in:1, in:1] and 
 	 * [in:1, out:2] are not valid, whereas [out:1, in:1] is. 
 	 */

	public void computeTopology() {
		
                topologyVertices = new HashSet<List<Object>>();
                topologyEdges = new HashSet<TopologyTransition>();
			
      		List<Object> initStates = new ArrayList<Object>();
                for(int i=0; i<resourceCount; i++) {
                	initStates.add(Constants.INITIAL_STATE_STR);
                }

		Set<List<Object>> visitedVertices = new HashSet<List<Object>>();
		computeTopologyInner(initStates, new ArrayList<ResourceTransition>(), 0, visitedVertices);
	}



	
	public void computeTopologyInner(List<Object> listOfVertices, List<ResourceTransition> edges, int currRes, Set<List<Object>> visitedVertices) {

		// Only check if the set of vertices have been visited
		// the very first time this method is called for a new
		// list of vertices. (Checking it at every recursive
		// call would be incorrect.)
		if(currRes == 0 && Helper.isIn(listOfVertices, visitedVertices)) {
			return;	
		}

		// If we've finished picking one transition per resource (for the
		// list of vertices being considered), and they are consistent...
		if(currRes == resourceCount && Helper.consistentEdges(edges)) {
   			List<Object> globalEndVertices = new ArrayList<Object>();
   			List<Object> globalLabels = new ArrayList<Object>();

			// Create the set of vertices that result from executing
			// the set of transitions that were picked, and combine 
			// their labels into one 'global' (topology) transition
                        for(ResourceTransition resourceEdge : edges) {
				globalEndVertices.add(resourceEdge.target);
				globalLabels.add(resourceEdge.label);
			}

                        topologyVertices.add(listOfVertices);
                        topologyVertices.add(globalEndVertices);
                        TopologyTransition tt = new TopologyTransition(listOfVertices,globalLabels,globalEndVertices); 
                        topologyEdges.add(tt);
			visitedVertices.add(listOfVertices);

			computeTopologyInner(globalEndVertices, new ArrayList<ResourceTransition>(), 0, visitedVertices);
		}

		// If we haven't yet finished picking a transition 
		// for every resource
		else if(currRes < resourceCount) {

			// Get a reference to all the transitions in resource 'currRes'
			List<Object> edgesOfResourceNum = (List<Object>)transOfAllResources[currRes];	
			
                        for(Object resourceEdge : edgesOfResourceNum) {
                        	ResourceTransition resourceEdge2 = (ResourceTransition)resourceEdge;

				// Look for outgoing transitions from the state being considered
				// for 'currRes'
				if(resourceEdge2.source.equals((String)listOfVertices.get(currRes))) {

					// We need to clone
					List<ResourceTransition> edgesNew = Helper.cloneEdges(edges); 
					edgesNew.add(resourceEdge2);
					computeTopologyInner(listOfVertices, edgesNew, currRes+1, visitedVertices);
				}
                        }
		}	
	}




	/**
 	 * The old topology computation, which relied on computing the cartesian product.
 	 * The function checks if two arbitrarily chosen global-source and global-target 
 	 * vertices have a valid transition between them. 
 	 **/
 
	public void computeTopologyOLD(List<List<Object>> globalVertices, List<List<Object>> globalLabels) {

		int resourceNum = 0;
		boolean notTransitionOfResource = false;

        	topologyVertices = new HashSet<List<Object>>();
        	topologyEdges = new HashSet<TopologyTransition>();

		for (List<Object> globalVtxSource : globalVertices) {
			
			for (List<Object> globalVtxTarget : globalVertices) {

				for (List<Object> globalLabel : globalLabels) {
			
					resourceNum = 0;
					notTransitionOfResource = false;

					String vtxTarget;
					String label;

					for (Object vtxSource : globalVtxSource) {

						vtxTarget = (String)globalVtxTarget.get(resourceNum);
						label = (String)globalLabel.get(resourceNum);
						List<Object> edgesOfResourceNum = (List<Object>)transOfAllResources[resourceNum];		
						ResourceTransition rt = new ResourceTransition((String)vtxSource, label, vtxTarget);
						if(!Helper.isIn(rt, new HashSet<Object>(edgesOfResourceNum),Constants.RESOURCE)) {
							notTransitionOfResource = true;
							break;
						}
						resourceNum++;
					}

					if(!notTransitionOfResource && !globalVtxSource.isEmpty()) {
					 	boolean	foundMatchingSync = true;

						for (Object label1 : globalLabel) {
							if(Helper.isSyncTask(label1)) {
								foundMatchingSync = false;
								for (Object label2 : globalLabel) {
									if(Helper.isSyncTask(label2) && Helper.oppositeSyncs(label1,label2) &&
										(Helper.number(label1) == Helper.number(label2))) {
										if(foundMatchingSync) {
											foundMatchingSync = false;
											break;
										}
										foundMatchingSync = true; 
									}	
								}
								if(!foundMatchingSync) {	
									break;
								}
							}
						}

						if(foundMatchingSync && !globalLabel.isEmpty()) {	
							topologyVertices.add(globalVtxSource);
							topologyVertices.add(globalVtxTarget);
							TopologyTransition tt = new TopologyTransition(globalVtxSource,globalLabel,globalVtxTarget);	
							if(!Helper.isIn(tt, topologyEdges, Constants.TOPOLOGY)) {
								topologyEdges.add(tt);
							}
						}
					}
				}
			}
		}
	}




	public Set<List<Object>> getTopologyVertices() {
		return topologyVertices;
	}




	public Set<TopologyTransition> getTopologyEdges() {
		return topologyEdges;
	}
}


