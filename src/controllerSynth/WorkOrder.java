package controllerSynth;

import javax.swing.*;
import javax.swing.event.*;
import java.awt.event.*;
import javax.swing.text.BadLocationException;
import javax.swing.border.Border;

import java.awt.*;
import java.io.*;
import java.util.*;
import java.util.stream.*;
import java.text.*;
import java.util.List;
import java.nio.file.*;
import java.nio.charset.*;

import com.mxgraph.swing.handler.mxRubberband;
import com.mxgraph.util.mxEvent;
import com.mxgraph.util.mxEventObject;
import com.mxgraph.util.mxEventSource.mxIEventListener;

import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.UIManager.*;

import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.*;

import com.mxgraph.model.*;
import com.mxgraph.layout.*;
import com.mxgraph.layout.orthogonal.*;
import com.mxgraph.layout.hierarchical.*;



public class WorkOrder extends JFrame 
{

	private static String algorithmToUse = "new";
	private static int defaultEdgeCounter = 0;
	private static boolean keepRecipeAutoArranged = false;
	private static boolean keepResourcesAutoArranged = false;
	private static boolean keepGraphsClipped = false;
	private static boolean debug = false;
	private static int defaultVertexCounter = 0; 
	private static int resourceCount = 0; 
	private java.util.Date timer;
	private static Object parent = null;
	private static Object [] resources = new Object[Constants.MAX_RESOURCES];
	private static Topology topology;
	private static Controller controller;

	private static Object [] transOfAllResources = new Object[Constants.MAX_RESOURCES];
	private static List<Object> [] verticesOfAllResources = new List[Constants.MAX_RESOURCES];
	private static List<Object> [] labelsOfAllResources = new List[Constants.MAX_RESOURCES];

	private static Set<List<Object>> topologyVertices = new HashSet<List<Object>>();
	private static Set<TopologyTransition> topologyEdges = new HashSet<TopologyTransition>();
	private static Set<ControllerTransition> controllerEdges = new HashSet<ControllerTransition>();

	private static Set<RecipeTransition> recipeEdges = new HashSet<RecipeTransition>();
	private static Set<String> recipeVertices = new HashSet<String>();
	private static Set<String> recipeLabels = new HashSet<String>();

	private	static final mxGraph graph = new mxGraph();
	private static mxGraphComponent graphComponent;
	private static JEditorPane edit;
	private static JFrame main;
	private static boolean syntaxError = false;
	public static JMenuItem createTopologyMenuItem;
	public static JMenuItem loadProjectMenuItem;
	public static JMenuItem displayTopologyMenuItem;
	public static JMenuItem displayControllerMenuItem;
	public static JMenuItem displayOpScheduleMenuItem;
	public static JButton createTopologyButton;
	public static JMenuItem checkManMenuItem;
	public static JLabel manufButton;
	public static JLabel syntaxErrButton;
	public static JMenuBar menuBar;
	public static JMenu menu;
	public static JMenuItem menuItem;
	public static WorkOrder frame;
	public static JFrame buttons;
	private static JSplitPane splitPane = null;
	private static File file = null;

	//JCC: We want a button to save the Recipe in a separate file, and a capability topology in a separate file.
	public static JMenuItem saveRecipeMenuItem;
	public static JMenuItem saveCapabilityTopologyMenuItem;
	//--


	public WorkOrder()
	{
		super(Constants.TOOLKIT_NAME);

		mxGraph graph = new mxGraph();
		parent = graph.getDefaultParent();

		graph.setStylesheet(Helper.createStylesheet(graph));
		graph.setAllowLoops(false);
		graph.setAutoSizeCells(true);
		graph.getModel().beginUpdate();

		try {}
		finally {
			graph.getModel().endUpdate();
		}

		graph.setAllowDanglingEdges(false);
		graph.setEdgeLabelsMovable(false);

		graphComponent = new mxGraphComponent(graph);
		((JComponent)graphComponent).setBorder(BorderFactory.createEmptyBorder());

		// Commented out for now as these don't seem to be needed
		// graphComponent.zoomIn();
		// graphComponent.zoomOut();




		/**
		 * Called whenever the recipe is changed or 'touched', 
		 * e.g. a state is moved to a slightly different location.
		 */

		graph.getModel().addListener(mxEvent.CHANGE, new mxIEventListener() {
			public void invoke(Object sender, mxEventObject evt) {	

				// Re-arrange the recipe-graph w.r.t. the predefined layout 
				autoArrange();

				// If the recipe has changed, we should recompute
				// the controller and manufacturability
				loadProjectMenuItem.setEnabled(false);
				displayControllerMenuItem.setEnabled(false);
				displayOpScheduleMenuItem.setEnabled(false);
				manufButton.setBackground(null);

				java.util.List lst = graph.findTreeRoots(parent);
				if(parent != null) {
					Object [] arr = new Object[1];
					arr[0] = parent; 
					Object[] cells = graph.getCellsForGroup(arr);
					cells = graph.getAllEdges(cells);

					// Check each transition label in the recipe 
					// to see if it has a syntax error, and set
					// the formatting/style of the edge as well 
					// as its label's text
					syntaxError = false;
					for( int i = 0; i <= cells.length-1; i++) {
						mxCell newCell = (mxCell)cells[i];
						if(newCell.getValue().equals(Constants.defaultEdgeRecipe) ||
								newCell.getValue().equals(Constants.EMPTYSTRING)) {

							newCell.setValue(Constants.defaultEdgeRecipe);
							newCell.setStyle(Constants.DEFAULT_EDGE);
						}
						else if(!((String)newCell.getValue()).matches(Constants.OP_EXPR_SYNTAX_REGEX)) {	
							newCell.setStyle(Constants.EDGE_SYNTAX_ERROR);
							syntaxError = true;
						}
						else {		
							newCell.setStyle(Constants.DEFAULT_EDGE);
						}
					}
					if(syntaxError) {
						syntaxErrButton.setBackground(Constants.SYNTAX_ERR_COLOR);
					}
					else {
						syntaxErrButton.setBackground(null);
					}

					graph.refresh();
				}

				graphComponent.validateGraph();
			}
		});

		// Initial validation
		graphComponent.validateGraph();
		new mxRubberband(graphComponent);
		getContentPane().add(graphComponent);
		graphComponent.getGraphControl().setFocusable(true);




		graphComponent.getGraphControl().addMouseListener(new MouseAdapter()
		{
			Object v;	
			public void mouseReleased(MouseEvent e) {
				mxCell cell = (mxCell)graphComponent.getCellAt(e.getX(), e.getY());

				if (cell != null && e.getClickCount() == 2 && cell.isEdge()) {		
					if(debug)
						System.out.println("cell="+graph.getLabel(cell));

					String edgeName = null;

					/**
					 * Iterate over each transition label's operation symbols 
					 * (which are separated by semicolons and/or parallelism) 
					 * and find their corresponding B2MML files in the folder
					 * 'B2MMLfolder'.
					 */

					try { 
						edgeName = graph.getLabel(cell);
						edgeName = Helper.reverse(edgeName);
						StringTokenizer toks = new StringTokenizer(edgeName,";|");
						edit.setEditorKit(new XMLEditorKit());
						while(toks.hasMoreTokens()) {
							String edgeSym = Helper.getSymbolOnly(toks.nextToken().trim());
							String xmlStr = Helper.readFile(Constants.B2MMLfolder+edgeSym+".xml");	
							edit.setText(Helper.removeWhiteSpaces(xmlStr));
						}
					}
					catch(Exception ee) {
						System.out.println("No matching XML file for "+edgeName);
					}
				}

				/**
				 * If there was a double-click on an empty spot within the panel,
				 * create a new vertex. Create an initial state if this is the 
				 * first vertex.
				 */

				else if (cell == null && e.getClickCount() == 2) {
					if(defaultVertexCounter == 0) {
						Object v = graph.insertVertex(parent, Constants.INITIAL_STATE, Constants.defaultVertex+defaultVertexCounter, e.getX(), e.getY(), Constants.DEFAULT_VERTEX_WIDTH, Constants.DEFAULT_VERTEX_HEIGHT, Constants.INITIAL_VERTEX);
					}
					else {
						Object v = graph.insertVertex(parent, Constants.DEFAULT_STATE, Constants.defaultVertex+defaultVertexCounter, e.getX(), e.getY(), Constants.DEFAULT_VERTEX_WIDTH, Constants.DEFAULT_VERTEX_HEIGHT, Constants.DEFAULT_VERTEX);
					}
					defaultVertexCounter++;			
					graphComponent.scrollCellToVisible(cell);
				}

				/**
				 * If the mouse was held on a vertex for longer than a certain
				 * duration, delete the vertex.
				 */

				if (cell != null && e.getClickCount() == 1 && timer != null) {
					long timeClicked = new Date().getTime() - timer.getTime();
					if (timeClicked >= Constants.DELETE_CELL_TIME) {
						timer = null;
						graph.removeCells(); 
					}
				}
			}

			public void mousePressed(MouseEvent e) {
				timer = new Date();
			}
		});
	}




	public JFrame getButtons() {
		return buttons;
	}




	public static void main(String[] args)
	{

		if (args.length > 0) {
			algorithmToUse = args[0];
		}

		Toolkit.getDefaultToolkit().setDynamicLayout(true);
		System.setProperty("sun.awt.noerasebackground", "true");
		JFrame.setDefaultLookAndFeelDecorated(true);
		JDialog.setDefaultLookAndFeelDecorated(true);

		try {
			/** The Nimbus look and feel **/
			//for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
			//	if ("Nimbus".equals(info.getName())) {
			// 		UIManager.setLookAndFeel(info.getClassName());
			//   	}
			//}

			/** Cross platform look and feel **/
			//UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());

			//UIManager.getSystemLookAndFeelClassName());

			UIManager.setLookAndFeel("de.muntjak.tinylookandfeel.TinyLookAndFeel"); 	
			Helper.setUIFont (new javax.swing.plaf.FontUIResource(Constants.UI_FONT,Font.PLAIN,Constants.UI_FONT_SIZE));

		}	 	
		catch(Exception e) {
			System.out.println("Something went wrong when setting the look and feel");
		}

		frame = new WorkOrder();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setUndecorated(true);
		frame.pack();

		buttons = new JFrame();

		JLabel infoPanelLabel = new JLabel(Constants.ALERTS_GUI_LABEL);
		buttons.add(infoPanelLabel);

		syntaxErrButton = new JLabel(Constants.SYNTAX_ERR_GUI_LABEL);
		syntaxErrButton.setOpaque(true);
		syntaxErrButton.setVisible(true);
		Border border1 = BorderFactory.createRaisedSoftBevelBorder();
		syntaxErrButton.setBorder(border1);

		buttons.setLayout(new FlowLayout());
		buttons.add(syntaxErrButton);
		manufButton = new JLabel(Constants.MANUF_GUI_LABEL);
		manufButton.setVisible(true);
		manufButton.setOpaque(true);
		Border border2 = BorderFactory.createRaisedSoftBevelBorder();
		manufButton.setBorder(border2);

		buttons.add(manufButton);
		frame.add(buttons.getContentPane(),BorderLayout.SOUTH);
		edit = new JEditorPane();
		edit.setBackground(null);
		edit.setOpaque(true);
		edit.setEditable(false);
		edit.setEditorKit(new XMLEditorKit());
		edit.setVisible(true);			
		frame.setTitle(Constants.TITLE);

		JTabbedPane tabbedPane = new JTabbedPane();
		tabbedPane.addTab(Constants.RECIPE_TAB, frame.getContentPane());
		tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);

		JPanel rightPane = new JPanel(new GridLayout(1,0));
		JScrollPane editScrollPane = new JScrollPane(edit);	
		editScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		editScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		rightPane.add(editScrollPane);
		rightPane.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(
						BorderFactory.createRaisedBevelBorder(),
						Constants.B2MML_WINDOW,
						javax.swing.border.TitledBorder.CENTER,
						javax.swing.border.TitledBorder.ABOVE_TOP),
				BorderFactory.createEmptyBorder(40,15,15,15)));

		JPanel leftPane = new JPanel(new GridLayout(1,0));
		leftPane.add(tabbedPane);
		leftPane.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(
						BorderFactory.createRaisedBevelBorder(),
						Constants.RECIPE_RESOURCES_WINDOW,
						javax.swing.border.TitledBorder.CENTER,
						javax.swing.border.TitledBorder.ABOVE_TOP),
				BorderFactory.createEmptyBorder(15,15,15,15)));

		splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, leftPane, rightPane); 
		splitPane.setOneTouchExpandable(true);
		splitPane.setDividerSize(15);


		main = new JFrame(Constants.TITLE);
		main.setLayout(new BorderLayout());	
		main.setSize(1000,500);

		menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		JMenu viewMenu = new JMenu("View");
		JMenu resourcesMenu = new JMenu("Resources");
		JMenu manuMenu = new JMenu("Manufacturability");
		JMenu help = new JMenu("Help");

		menuBar.add(fileMenu);
		menuBar.add(viewMenu);
		menuBar.add(resourcesMenu);
		menuBar.add(manuMenu);
		menuBar.add(help);

		JMenuItem addResMenuItem = new JMenuItem("Add Resource");	
		loadProjectMenuItem = new JMenuItem("Load Project");
		JMenuItem saveProjectMenuItem = new JMenuItem("Save Project");
		JMenuItem quitMenuItem = new JMenuItem("Quit");
		createTopologyMenuItem = new JMenuItem("Create Topology");
		createTopologyMenuItem.setEnabled(false);
		checkManMenuItem = new JMenuItem("Check Manufacturability");
		checkManMenuItem.setEnabled(false);
		displayTopologyMenuItem = new JMenuItem("Display Topology");
		displayTopologyMenuItem.setEnabled(false);
		displayControllerMenuItem = new JMenuItem("Display Controller");
		displayControllerMenuItem.setEnabled(false);
		displayOpScheduleMenuItem = new JMenuItem("Display Operations Schedule");
		displayOpScheduleMenuItem.setEnabled(false);
		JMenuItem zoomInMenuItem = new JMenuItem("Zoom In");
		JMenuItem zoomOutMenuItem = new JMenuItem("Zoom Out");

		zoomInMenuItem.setMnemonic(KeyEvent.VK_I);
		zoomOutMenuItem.setMnemonic(KeyEvent.VK_O);
		zoomOutMenuItem.setDisplayedMnemonicIndex(5);

		zoomInMenuItem.setAccelerator(KeyStroke.getKeyStroke(
				java.awt.event.KeyEvent.VK_I, 
				java.awt.Event.CTRL_MASK));

		zoomOutMenuItem.setAccelerator(KeyStroke.getKeyStroke(
				java.awt.event.KeyEvent.VK_O, 
				java.awt.Event.CTRL_MASK));

		//JMenuItem checkManuMenuItem = new JMenuItem("Check Manufacturability");
		JCheckBoxMenuItem clipLabelsMenuItem = new JCheckBoxMenuItem("Clip Labels");
		JCheckBoxMenuItem autoArrangeRecipeMenuItem = new JCheckBoxMenuItem("Keep Recipe Arranged");
		JCheckBoxMenuItem autoArrangeResourcesMenuItem = new JCheckBoxMenuItem("Keep Resources Arranged");

		fileMenu.add(loadProjectMenuItem);
		fileMenu.add(saveProjectMenuItem);

		//JCC: Add buttons for saving parts of file
		saveRecipeMenuItem = new JMenuItem("Save Recipe");
		saveCapabilityTopologyMenuItem = new JMenuItem("Save Capability Topology");
		fileMenu.add(saveRecipeMenuItem);
		fileMenu.add(saveCapabilityTopologyMenuItem);
		//--

		fileMenu.add(quitMenuItem);

		viewMenu.add(zoomInMenuItem);
		viewMenu.add(zoomOutMenuItem);
		viewMenu.add(clipLabelsMenuItem);
		viewMenu.add(autoArrangeRecipeMenuItem);
		viewMenu.add(autoArrangeResourcesMenuItem);

		resourcesMenu.add(addResMenuItem);
		resourcesMenu.add(createTopologyMenuItem);
		resourcesMenu.add(displayTopologyMenuItem);

		manuMenu.add(checkManMenuItem);
		manuMenu.add(displayControllerMenuItem);
		manuMenu.add(displayOpScheduleMenuItem);




		addResMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Resource tabNew = new Resource(createTopologyMenuItem, displayTopologyMenuItem, displayControllerMenuItem, displayOpScheduleMenuItem, checkManMenuItem, manufButton);
				tabNew.getButtons().add(manufButton);

				try {
					tabbedPane.remove(controller.getContentPane());
				} catch(Exception ee) {}

				try {
					tabbedPane.remove(topology.getContentPane());
				} catch(Exception ee) {}

				loadProjectMenuItem.setEnabled(false);
				createTopologyMenuItem.setEnabled(false);
				displayTopologyMenuItem.setEnabled(false);
				displayControllerMenuItem.setEnabled(false);
				displayOpScheduleMenuItem.setEnabled(false);
				checkManMenuItem.setEnabled(false);
				manufButton.setBackground(null);

				String cmd = e.getActionCommand();
				if(cmd.equals(Constants.ADD_RESOURCE_TAB)) {
					tabbedPane.addTab(Constants.RESOURCE_TAB+" "+(resourceCount+1),tabNew.getContentPane());		
					tabbedPane.setSelectedIndex(resourceCount+1);
					resources[resourceCount] = tabNew;	
					tabNew.autoArrange(keepResourcesAutoArranged);
					resourceCount++;
				}
			}
		});




		tabbedPane.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {

				JTabbedPane sourceTabbedPane = (JTabbedPane) e.getSource();
				int index = sourceTabbedPane.getSelectedIndex();
				String title = sourceTabbedPane.getTitleAt(index);
				if(title.startsWith(Constants.RESOURCE_TAB)) {
					if(index <= resourceCount) {
						((Resource)resources[index-1]).getButtons().add(manufButton);	
					}
				}
				else if(title.startsWith(Constants.RECIPE_TAB)) {
					buttons.add(manufButton);
					graphComponent.refresh();

				}
				else if(title.startsWith(Constants.CONTROLLER_TAB)) {
					controller.getButtons().add(manufButton);	
				}
				else if(title.startsWith(Constants.TOPOLOGY_TAB)) {
					topology.getButtons().add(manufButton);	
				}
			}
		});




		displayTopologyMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Topology tabNew = new Topology(topologyEdges, manufButton, resourceCount);
				topology = tabNew;
				tabNew.getButtons().add(manufButton);

				String cmd = e.getActionCommand();
				if(cmd.equals("Display Topology")) {
					int topTabIndex = tabbedPane.indexOfTab(Constants.TOPOLOGY_TAB);
					if(topTabIndex == -1) {
						int tabNum = resourceCount+1;
						tabbedPane.addTab(Constants.TOPOLOGY_TAB,tabNew.getContentPane());
						tabbedPane.setSelectedIndex(tabNum);
					}
					else {	
						tabbedPane.setComponentAt(topTabIndex,tabNew.getContentPane());
						tabbedPane.setSelectedIndex(topTabIndex);
					}
				}
				tabNew.clipLabels(keepGraphsClipped);
			}
		});




		displayControllerMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Controller tabNew = new Controller(controllerEdges, manufButton, resourceCount);
				controller = tabNew;
				tabNew.getButtons().add(manufButton);

				String cmd = e.getActionCommand();
				if(cmd.equals("Display Controller")) {
					int conTabIndex = tabbedPane.indexOfTab(Constants.CONTROLLER_TAB);
					if(conTabIndex == -1) {
						int tabNum = tabbedPane.getTabCount(); 
						tabbedPane.addTab(Constants.CONTROLLER_TAB, tabNew.getContentPane());
						tabbedPane.setSelectedIndex(tabNum);
					}
					else {
						tabbedPane.setComponentAt(conTabIndex, tabNew.getContentPane());
						tabbedPane.setSelectedIndex(conTabIndex);
					}
					tabNew.clipLabels(keepGraphsClipped);
				}
			}
		});




		displayOpScheduleMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				OperationsSchedule opSched = new OperationsSchedule(controllerEdges, resourceCount);

				String xmlStr = opSched.toString();
				edit.setEditorKit(new XMLEditorKit());
				edit.setText(xmlStr);
			}
		});




		quitMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				int result = JOptionPane.showConfirmDialog(main,
						"Save current project?",
						"Confirm", JOptionPane.YES_NO_CANCEL_OPTION);

				if(result == JOptionPane.YES_OPTION) {
					if(file == null) {
						file = Helper.getSelectedFile(file, main);
						if(file == null) {
							return;
						}
					}

					saveCurrentRecipe();
					saveCurrentResources();
					saveInFile(file.getPath());
					try {Thread.sleep(500); } catch(Exception ee) {}
					System.exit(0);
				}
				else if(result == JOptionPane.NO_OPTION) {
					try {Thread.sleep(500); } catch(Exception ee) {}
					System.exit(0);
				}
			}
		});




		zoomInMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int selectedTab = tabbedPane.getSelectedIndex();
				String title = tabbedPane.getTitleAt(selectedTab);

				if(title.startsWith(Constants.RECIPE_TAB)) {
					graphComponent.zoomIn();
				}
				else if(title.startsWith(Constants.RESOURCE_TAB)) {
					((Resource)resources[selectedTab-1]).zoomIn();
				}
				else if(title.startsWith(Constants.TOPOLOGY_TAB)) {
					topology.zoomIn();
				}
				else if(title.startsWith(Constants.CONTROLLER_TAB)) {
					controller.zoomIn();
				}
			}
		});




		clipLabelsMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AbstractButton aButton = (AbstractButton) e.getSource();
				boolean selected = aButton.getModel().isSelected();

				int selectedTab = tabbedPane.getSelectedIndex();
				String title = tabbedPane.getTitleAt(selectedTab);

				clipLabels(selected);
				for(int i=0; i<resourceCount; i++) {
					((Resource)resources[i]).clipLabels(selected);
				}
				if(topology != null)
					topology.clipLabels(selected);
				if(controller != null)
					controller.clipLabels(selected);
			}
		});




		autoArrangeRecipeMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AbstractButton aButton = (AbstractButton) e.getSource();
				boolean selected = aButton.getModel().isSelected();

				//System.out.println("Arrange clicked "+selected);
				keepRecipeAutoArranged = selected; 

				autoArrange(selected);
			}
		});




		autoArrangeResourcesMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AbstractButton aButton = (AbstractButton) e.getSource();
				boolean selected = aButton.getModel().isSelected();

				//System.out.println("Arrange clicked "+selected);

				keepResourcesAutoArranged = selected; 
				for(int i=0; i<resourceCount; i++) {
					((Resource)resources[i]).autoArrange(selected);
				}
			}
		});




		zoomOutMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int selectedTab = tabbedPane.getSelectedIndex();
				String title = tabbedPane.getTitleAt(selectedTab);

				if(title.startsWith(Constants.RECIPE_TAB)) {
					graphComponent.zoomOut();
				}
				else if(title.startsWith(Constants.RESOURCE_TAB)) {
					((Resource)resources[selectedTab-1]).zoomOut();
				}
				else if(title.startsWith(Constants.TOPOLOGY_TAB)) {
					topology.zoomOut();
				}
				else if(title.startsWith(Constants.CONTROLLER_TAB)) {
					controller.zoomOut();
				}
			}
		});




		loadProjectMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				List<String> lines = null;
				JFileChooser fc = new JFileChooser();
				fc.setCurrentDirectory(new File("."));
				FileNameExtensionFilter filter = new FileNameExtensionFilter("Work Order Projects (*.wop)", "wop", "wop");
				fc.setFileFilter(filter);

				int result = fc.showOpenDialog(main);

				if (result != JFileChooser.APPROVE_OPTION) {
					//System.out.println("You cancelled the choice");
					return;
				}

				file = fc.getSelectedFile();

				loadProjectMenuItem.setEnabled(false);
				try {
					lines = Files.readAllLines(file.toPath(), Charset.forName("UTF-8"));
				}
				catch (IOException fe) {
					System.out.println("File read unsuccessful");		
				}

				int i=0;
				int resourceCountLocal = 0;
				Hashtable<String, Object> vertexTable = new Hashtable<String, Object>();


				for(String line : lines) {
					if(line.startsWith(Constants.RESOURCE_COUNT)) {
						String resNum = line.substring(Constants.RESOURCE_COUNT.length()).trim();
						resourceCountLocal = Integer.parseInt(resNum);

					}
					if(line.equals(Constants.START_OF_RECIPE_EDGES_IN_FILE)) {
						break;
					}
					if(line.startsWith(Constants.INITIAL_STATE_STR)) {
						Point p = new Point(lines.get(i+1));
						Object v = graph.insertVertex(parent, Constants.INITIAL_STATE, line, p.getX(), p.getY(), Constants.DEFAULT_VERTEX_WIDTH, Constants.DEFAULT_VERTEX_HEIGHT, Constants.INITIAL_VERTEX);
						vertexTable.put(line,v);
						defaultVertexCounter++;
					}
					else if(line.startsWith(Constants.defaultVertex)) {
						Point p = new Point(lines.get(i+1));
						Object v = graph.insertVertex(parent, Constants.DEFAULT_STATE, line, p.getX(), p.getY(), Constants.DEFAULT_VERTEX_WIDTH, Constants.DEFAULT_VERTEX_HEIGHT, Constants.DEFAULT_VERTEX);
						vertexTable.put(line,v);
						defaultVertexCounter++;
					}
					i++;
				}	


				int j=0;

				for(String line : lines) {
					if(line.equals(Constants.START_OF_RESOURCE_VERTICES_IN_FILE)) {
						break;
					}
					if(line.equals(Constants.START_OF_RECIPE_EDGE_IN_FILE)) {
						graph.insertEdge(parent, null, lines.get(j+2), vertexTable.get(lines.get(j+1)), vertexTable.get(lines.get(j+3)),Constants.DEFAULT_EDGE);
					}
					j++;
				}

				graphComponent.refresh();


				for(int k=0; k<resourceCountLocal; k++) {
					Resource tabNew = new Resource(createTopologyMenuItem, displayTopologyMenuItem, displayControllerMenuItem, displayOpScheduleMenuItem, checkManMenuItem, manufButton, file.getPath(), k);
					tabNew.getButtons().add(manufButton);


					tabbedPane.addTab(Constants.RESOURCE_TAB+" "+(k+1),tabNew.getContentPane());
					tabbedPane.setSelectedIndex(k+1);
					resources[k] = tabNew;
					tabNew.autoArrange(keepResourcesAutoArranged);
					resourceCount++;
				}

				autoArrange();		
				saveCurrentRecipe();
				saveCurrentResources();
				createTopologyMenuItem.setEnabled(true);
				main.setTitle(Constants.TITLE + " - " + file.getPath());
			}
		});




		saveProjectMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	

				file = Helper.getSelectedFile(file, main);
				if(file == null) 
					return;	

				saveCurrentRecipe();
				autoArrange();
				saveCurrentResources();
				createTopologyMenuItem.setEnabled(true);
				saveInFile(file.getPath());
			}
		});




		createTopologyMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<List<Object>> listOfListOfVertices = Arrays.asList(verticesOfAllResources); 
				List<List<Object>> listOfListOfLabels = Arrays.asList(labelsOfAllResources); 

				int result = JOptionPane.showConfirmDialog(main,
						"Computing the topology might take a while. Continue?", 
						"Confirm", JOptionPane.OK_CANCEL_OPTION);

				if(result == JOptionPane.YES_OPTION) {
					createTopologyMenuItem.setEnabled(false);
					displayTopologyMenuItem.setEnabled(false);

					DialogThread dt = new DialogThread(main, "Computing topology");
					Thread th = new Thread(dt);
					th.start();

					new Thread() {
						public void run() {
							TopologyMethods topMeth = new TopologyMethods(resourceCount, transOfAllResources);

							long startTime = System.currentTimeMillis();

							if(algorithmToUse.equalsIgnoreCase("new")) {
								topMeth.computeTopology();
							}
							else {
								// Old topology computation algorithm.
								topMeth.computeTopologyOLD(topMeth.cartesianProduct(listOfListOfVertices),
										topMeth.cartesianProduct(listOfListOfLabels));
							}

							long stopTime = System.currentTimeMillis();
							long elapsedTime = stopTime - startTime;

							DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
							Date date = new Date();
							System.out.println("\nCurrent time & date: "+dateFormat.format(date)); 

							System.out.println("Topology computed in: "+elapsedTime+" ms");

							topologyVertices = topMeth.getTopologyVertices();
							topologyEdges = topMeth.getTopologyEdges();

							String time = new String(dt.getTime());
							dt.interrupt();

							long vertices = topologyVertices.size();
							long edges = topologyEdges.size();

							System.out.println("Topology has: "+ vertices +" states and " + edges +" transitions");

							JOptionPane.showConfirmDialog(main,
									"The topology was computed in "+time+
									".\nIt has "+ vertices +" states and "
									+ edges +" transitions.",
									"Message", JOptionPane.DEFAULT_OPTION);


							createTopologyMenuItem.setText("Create Topology");
							createTopologyMenuItem.setEnabled(true);
							displayTopologyMenuItem.setEnabled(true);

							if(debug) {
								System.out.println("\n----VERTICES----");
								System.out.println(topologyVertices);
								System.out.println("\n\n----EDGES----");
								System.out.println(topologyEdges);
							}

							checkManMenuItem.setEnabled(true);
						}
					}.start();
				}
			}
		});
		
		/**
		 * =======================================================================
		 * Jack's Edits
		 * Reuse above saveProject code for saving the recipe.
		 */
		saveRecipeMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				File saveLocation = Helper.getSelectedFile(null, main);
				
				//If no file was chosen...
				if (saveLocation == null) {
					JOptionPane.showMessageDialog(
							main,																//Parent frame
							"No Recipe saved as no file location was chosen.",					//Message
							"No File location selected.",										//Heading
							JOptionPane.ERROR_MESSAGE											//Type
							);
					return;					
				}
				
				saveCurrentRecipe();
				autoArrange();
				saveRecipeInFile(saveLocation.getPath());				
			}		
		});
		
		saveCapabilityTopologyMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				//Save current state of resources.
				autoArrange();
				saveCurrentResources();
				
				//only do this is there is at least 1 capability resource.
				if (resourceCount == 0) {
					JOptionPane.showMessageDialog(
							main,																//Parent frame
							"You cannot save a capability topology if no resources exist.",		//Message
							"Could not save Capability Topology.",								//Heading
							JOptionPane.ERROR_MESSAGE											//Type
							);
					return;
				}
				
				//Get all possible capability numbers
				String[] possibilities = new String[resourceCount];
				for (int i = 0; i < resourceCount; i++) {
					possibilities[i] = (String.valueOf(i));
				}					
				
				//Open Dialog to Select Capability
				String choice = (String)JOptionPane.showInputDialog(
						main,																	//Parent frame
						"Select Resource for which the\n"
						+ "Capability Topology will be saved:\n"
						+ "PAY ATTENTION TO INDEXING NOT NAME. ZERO INDEXED",					//Question
						"Save Capability Topology",												//Heading
						JOptionPane.INFORMATION_MESSAGE,										//Type
						null,																	//No icon
						possibilities,															//Choices
						possibilities[0]														//Default choice
						);
				
				//If no value was chosen...
				if (choice == null || choice == "") {
					JOptionPane.showMessageDialog(
							main,																//Parent frame
							"No Capability Topology saved as none were selected.",				//Message
							"No resource selected.",											//Heading
							JOptionPane.ERROR_MESSAGE											//Type
							);
					return;
				}
				
				//Get integer of choice.
				int choiceNumber = Integer.valueOf(choice);
				
				//Save topology
				File saveLocation = Helper.getSelectedFile(null, main);
				
				//If no file was chosen...
				if (saveLocation == null) {
					JOptionPane.showMessageDialog(
							main,																//Parent frame
							"No Capability Topology saved as no file location was chosen.",		//Message
							"No File location selected.",										//Heading
							JOptionPane.ERROR_MESSAGE											//Type
							);
					return;					
				}
				
				//Otherwise, save away.
				saveCapabilityTopologyToFile(saveLocation.getPath(), choiceNumber);
				
				//Save Execution Shell
				File shellSaveLocation = Helper.getSelectedFile(null, main);
				
				//If no file was chosen...
				if (shellSaveLocation == null) {
					JOptionPane.showMessageDialog(
							main,																//Parent frame
							"No Execution Code Shell saved as no file location was chosen.",	//Message
							"No File location selected.",										//Heading
							JOptionPane.ERROR_MESSAGE											//Type
							);
					return;					
				}
				
				//Otherwise save shell.
				saveExecutionCodeShellToFile(shellSaveLocation.getPath(), choiceNumber);
			
			}				
		});
		
		
		/**
		 * =======================================================================
		 */




		// TODO:need to find the initial states automatically
		checkManMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// temporary - for testing
				List<Object> a = new ArrayList<Object>();
				for(int i=0; i<resourceCount; i++) {
					a.add(Constants.INITIAL_STATE_STR);
				}
				Sigma sigma = new Sigma(a, new ResourceVector());
				manufButton.setBackground(null);


				DialogThread dt = new DialogThread(main, "Checking manufacturability");
				Thread th = new Thread(dt);
				th.start();

				if(debug)
					System.out.println("\nChecking manufacturability\n");

				new Thread() {
					public void run() {
						SimulationMethods simMeth = new SimulationMethods(recipeEdges, 
								topologyEdges, 
								resourceCount, 
								debug);

						Object [] res = simMeth.findSim(sigma,Constants.INITIAL_STATE_STR);

						String time = new String(dt.getTime());
						dt.interrupt();

						if(((Set)res[0]).size()  > 0) {
							manufButton.setBackground(Constants.MANUF_COLOR);
							displayControllerMenuItem.setEnabled(true);
							displayOpScheduleMenuItem.setEnabled(true);
						}
						else {
							manufButton.setBackground(Constants.NOT_MANUF_COLOR);
						}

						JOptionPane.showConfirmDialog(main,
								"Manufacturability checked in "+time,
								"Message", JOptionPane.DEFAULT_OPTION);

						controllerEdges = (Set<ControllerTransition>)res[1];

						if(debug) {
							System.out.println("\nSIMULATION TRIPLES");

							for (SimulationTriple tup : (Set<SimulationTriple>)res[0]) {
								System.out.println(tup.toString(resourceCount));
							}
							System.out.println("\nCONTROLLER TRANSITIONS");
							for (ControllerTransition tup : (Set<ControllerTransition>)res[1]) {
								System.out.println(tup.toString(resourceCount));
							}
						}
					}
				}.start();
			}
		});

		main.setJMenuBar(menuBar);
		main.add(splitPane,BorderLayout.CENTER);      
		main.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		main.setVisible(true);
		splitPane.setDividerLocation(Constants.LEFT_PANEL_PERCENTAGE);
	}




	public static void clipLabels(boolean clip) {
		keepGraphsClipped = clip;
		clipLabels(); 
	}




	public static void clipLabels() {
		graph.setLabelsClipped(keepGraphsClipped);
		graphComponent.refresh();
	}




	public static void autoArrange(boolean arrange) {
		keepRecipeAutoArranged = arrange;
		autoArrange();
		//temporary solution to update graph (to show new layout)
		//graphComponent.zoomIn();
		//graphComponent.zoomOut();
	}




	public static void autoArrange() {

		if(keepRecipeAutoArranged) {
			//System.out.println("Setting hierarchical layout");
			mxHierarchicalLayout layout = new mxHierarchicalLayout(graph,SwingConstants.WEST);
			layout.setFineTuning(true);
			layout.setInterRankCellSpacing(200);
			layout.setIntraCellSpacing(100);
			layout.execute(parent);
		}
		else {
			mxParallelEdgeLayout layout = new mxParallelEdgeLayout(graph, Constants.PARALLEL_EDGE_OFFSET);
			layout.execute(parent);
		}
		clipLabels(); 
	}




	/**
	 * =======================================================================
	 * Jack Edits
	 * Refactoring to allow saving of subsets of the project
	 */
	
	public static void saveInFile(String filename) {

		try {
			List<String> lines = new ArrayList<String>();
			lines.add(Constants.RESOURCE_COUNT+" "+resourceCount);
			
			//Get all recipe Lines
			lines.addAll(getLinesOfRecipe());

			for(int resNum=0; resNum<resourceCount; resNum++) {		
				lines.add(" ");
				lines.addAll(getLinesOfResource(resNum));			
			}

			Path file = Paths.get(filename);
			Files.write(file, lines, Charset.forName("UTF-8"));
		} 
		catch (IOException e) {
			System.out.println("File write unsuccessful");
		}

	}
	
	/**
	 * Get string lines of the recipe. This prevents code duplication. 
	 * @return
	 */	
	public static ArrayList<String> getLinesOfRecipe() {
		
		ArrayList<String> lines = new ArrayList<String>();
		
		lines.add(Constants.START_OF_RECIPE_VERTICES_IN_FILE);

		for(String recipeVertex : recipeVertices) {
			lines.add(recipeVertex);
			lines.add(getXY(recipeVertex).toString());
		}
		lines.add(" ");

		lines.add(Constants.START_OF_RECIPE_EDGES_IN_FILE);
		for(RecipeTransition recipeEdge : recipeEdges) {
			lines.add(Constants.START_OF_RECIPE_EDGE_IN_FILE);
			lines.add(recipeEdge.source);
			lines.add(recipeEdge.label.label);
			lines.add(recipeEdge.target);
		}
		
		return lines;
	}
	
	/**
	 * Get string lines of a specific resource. (Again to prevent code duplication)
	 */
	public static ArrayList<String> getLinesOfResource(int resNum) {
		
		ArrayList<String> lines = new ArrayList<String>();
		
		lines.add(Constants.START_OF_RESOURCE_VERTICES_IN_FILE+" "+resNum);
		
		for(Object resourceVertex : verticesOfAllResources[resNum]) {
			lines.add(resourceVertex.toString());
			lines.add(((Resource)resources[resNum]).getXY(resourceVertex.toString()).toString());
		}

		lines.add(" ");
		lines.add(Constants.START_OF_RESOURCE_EDGES_IN_FILE+" "+resNum);
		
		for(Object resourceEdge : (List<Object>)transOfAllResources[resNum]) {
			ResourceTransition resourceEdge2 = (ResourceTransition)resourceEdge;
			lines.add(Constants.START_OF_RESOURCE_EDGE_IN_FILE);
			lines.add(resourceEdge2.source);
			lines.add(resourceEdge2.label);
			lines.add(resourceEdge2.target);
		}
		
		return lines;		
	}
	
	/**
	 * Save just the recipe to file.
	 * @param fileName
	 */
	
	public static void saveRecipeInFile(String fileName) {
		
		List<String> lines = new ArrayList<String>();
		lines.addAll(getLinesOfRecipe());
		
		try {
			Path file = Paths.get(fileName);
			Files.write(file, lines, Charset.forName("UTF-8"));
		} catch (IOException e) {
			System.out.println("File write unsuccessful");
		}		
	}
	
	/**
	 * Save just a capability topology to file.
	 */
	public static void saveCapabilityTopologyToFile(String fileName, int resourceNumber) {
		
		List<String> lines = new ArrayList<String>();
		lines.addAll(getLinesOfResource(resourceNumber));
		
		try {
			Path file = Paths.get(fileName);
			Files.write(file, lines, Charset.forName("UTF-8"));
		} catch (IOException e) {
			System.out.println("File write unsuccessful");
		}		
	}
	
	/**
	 * Save a shell for execution codes to file.
	 */
	public static void saveExecutionCodeShellToFile(String fileName, int resourceNumber) {
		
		List<String> initialLines = new ArrayList<String>();
		initialLines.addAll(getLinesOfResource(resourceNumber));
		
		List<String> outputLines = new ArrayList<String>();
		boolean edgesFound = false;
		//For every line in the capability topology.
		for (int i = 0; i < initialLines.size(); i++) {
			System.out.println("In For Loops");
			//Don't care about states and their locations
			if (!edgesFound && initialLines.get(i).startsWith(Constants.START_OF_RESOURCE_EDGES_IN_FILE)) {
				edgesFound = true;
				System.out.println("edgesFound = true");
			} else if (!edgesFound) {
				continue;
			}
			
			//For each edge:
			if (initialLines.get(i).equalsIgnoreCase(Constants.START_OF_RESOURCE_EDGE_IN_FILE)) {
				System.out.println("Found an edge");
				//Write header
				outputLines.add(initialLines.get(i++));
				//Write start state label
				outputLines.add(initialLines.get(i++));
				//Store and write label
				String labelName = initialLines.get(i);
				outputLines.add(initialLines.get(i++));
				//Write end state label.
				outputLines.add(initialLines.get(i));
				//Write holder for execution code.
				outputLines.add("executionCode=<CODE_FOR_OP_" + labelName + ">");	
				outputLines.add("");
			} else {
				outputLines.add(initialLines.get(i));
			}
		}
		
		try {
			Path file = Paths.get(fileName);
			Files.write(file, outputLines, Charset.forName("UTF-8"));
		} catch (IOException e) {
			System.out.println("File write unsuccessful");
		}
	}
	
	/**
	 * =======================================================================
	 */




	public static Point getXY(String vertex) {

		Object [] arr = new Object[1];
		arr[0] = parent;
		Object[] cells = graph.getCellsForGroup(arr);
		cells = graph.getAllEdges(cells);

		for(int i = 0; i <= cells.length-1; i++) {
			mxCell newCell = (mxCell)cells[i];
			String vertex1 = (newCell.getSource()).getValue().toString();
			String vertex2 = (newCell.getTarget()).getValue().toString();

			if(vertex1.equals(vertex)) {
				return new Point((newCell.getSource()).getGeometry().toString());
			}
			else if(vertex2.equals(vertex)) {
				return new Point((newCell.getTarget()).getGeometry().toString());
			}
		}

		return null;
	}




	public static void saveCurrentResources() {

		for(int res=0; res < resourceCount; res++) {
			verticesOfAllResources[res] = null; //new ArrayList<Object>();
			transOfAllResources[res] = null;
			labelsOfAllResources[res] = null;
		}

		for(int res=0; res < resourceCount; res++) {
			((Resource)resources[res]).saveCurrentResource();
			List<Object> listVertices = new ArrayList( ((Resource)resources[res] ).getVertices());
			verticesOfAllResources[res] = listVertices;
			List<Object> listEdges = new ArrayList( ((Resource)resources[res] ).getEdges());
			transOfAllResources[res] = listEdges;
			List<Object> listLabels = new ArrayList( ((Resource)resources[res] ).getLabels());
			labelsOfAllResources[res] = listLabels;
		}
	}




	public static void saveCurrentRecipe() {

		Object [] arr = new Object[1];
		arr[0] = parent;
		Object[] cells = graph.getCellsForGroup(arr);
		cells = graph.getAllEdges(cells);

		recipeEdges = new HashSet<RecipeTransition>();
		recipeVertices = new HashSet<String>();
		recipeLabels = new HashSet<String>();

		for( int i = 0; i < cells.length; i++) {
			mxCell newCell = (mxCell)cells[i];

			String vertex1 = (newCell.getSource()).getValue().toString();
			String vertex2 = (newCell.getTarget()).getValue().toString();
			RecipeTransition rt = new RecipeTransition(vertex1, new OperationExpression(newCell.getValue().toString()), vertex2);
			if(!Helper.isIn(rt, recipeEdges, Constants.RECIPE)) {
				recipeEdges.add(rt);
			}
			if(!Helper.isIn(vertex1,recipeVertices))
				recipeVertices.add(vertex1);
			if(!Helper.isIn(vertex2,recipeVertices))
				recipeVertices.add(vertex2);
			if(!Helper.isIn(newCell.getValue().toString(),recipeLabels))
				recipeLabels.add(newCell.getValue().toString());
		}
	}




	public static ImageIcon createImageIcon(String path) {
		java.net.URL imgURL = WorkOrder.class.getResource(path);
		if (imgURL != null) {
			return new ImageIcon(imgURL);
		} else {
			System.err.println("Couldn't find file: " + path);
			return null;
		}
	}

}
