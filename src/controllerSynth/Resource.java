package controllerSynth;
//package com.mxgraph.examples.swing;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.event.*;
import javax.swing.text.BadLocationException;
import javax.swing.border.Border;
//import java.awt.event.ActionListener;
//import java.awt.event.ActionEvent;
import java.awt.*;
import java.io.*;
//import java.awt.event.FocusEvent;
//import java.awt.event.FocusListener;
import java.util.*;
import java.util.List;
import java.nio.file.*;
import java.nio.charset.*;



import com.mxgraph.swing.handler.mxRubberband;
import com.mxgraph.util.mxEvent;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxEventObject;
import com.mxgraph.util.mxEventSource.mxIEventListener;

import javax.swing.JFrame;

import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.*;
import com.mxgraph.model.*;
import com.mxgraph.layout.*;
import com.mxgraph.layout.hierarchical.*;



public class Resource extends JFrame
{
	
	/**
	 * 
	 */
	//private static final long serialVersionUID = -2764911804288120883L;
	//private static final String circle = "shape=ellipse;perimeter=ellipsePerimeter";
	//private static final String B2MMLfolder = "B2MML/";
	//private static final String defaultVertex = "State";
	////private static final String NOP = "nop";
	//private static final String defaultEdge = Constants.NOP; 
	//private static final String TITLE = "Work Order Development Toolkit"; 
	//private static String b2mmlViewer = "B2MML Viewer"; 
	//private static final String INSYNC = "in:"; 
	//private static final String OUTSYNC = "out:"; 
	//private static final String IN_OUT_COLOR = "fontColor=#DC143C"; 
	//private static final String NOP_COLOR = "fontColor=#00C5CD"; 
	//private static final String DEFAULT_COLOR = "fontColor=#000000"; 

        private boolean keepResourceAutoArranged = false;
        private static boolean keepGraphsClipped = false;

        //private mxHierarchicalLayout layout;
	private int defaultEdgeCounter = 0;
	private int defaultVertexCounter = 0; 
        private java.util.Date timer;
 	private Object parent = null;
	private Set<String> vertices = new HashSet<String>();
	private Set<String> labels = new HashSet<String>();
	private Set<ResourceTransition> edges = new HashSet<ResourceTransition>();
	private mxGraph graph;
        private mxGraphComponent graphComponent;

	
	private static JEditorPane edit;

	public static boolean syntaxError = false;
	public static JMenuItem createTopologyMenuItem;
	public static JMenuItem displayTopMenuItem;
	public static JMenuItem displayContMenuItem;
	public static JMenuItem displayOpSchedMenuItem;
	public static JMenuItem checkManMenuItem;
	private JFrame buttons;
	private JLabel manufButton;

	public Resource(JMenuItem createTop, JMenuItem displayTop, JMenuItem displayCont, JMenuItem displayOpSched, JMenuItem checkMan, JLabel manButton, String fileName, int resCnt) {

		this(createTop, displayTop, displayCont, displayOpSched, checkMan, manButton);

                List<String> lines = null;

                try {
                	lines = Files.readAllLines(new File(fileName).toPath(), Charset.forName("UTF-8"));
                }
                catch (IOException fe) {
                	System.out.println("File read unsuccessful");
                }

                int i=0;
                Hashtable<String, Object> vertexTable = new Hashtable<String, Object>();

		boolean foundResVertices = false;
                for(String line : lines) {
                        if(line.startsWith(Constants.START_OF_RESOURCE_EDGES_IN_FILE+" "+resCnt)) {
                        	break;
                        }
                        if(line.startsWith(Constants.START_OF_RESOURCE_VERTICES_IN_FILE+" "+resCnt)) {
				foundResVertices = true;
			}
                        if(line.startsWith("state0") && foundResVertices) {
                        	Point p = new Point(lines.get(i+1));
                                Object v = graph.insertVertex(parent, Constants.INITIAL_STATE, line, p.getX(), p.getY(), Constants.DEFAULT_VERTEX_WIDTH, Constants.DEFAULT_VERTEX_HEIGHT, Constants.INITIAL_VERTEX);
                                vertexTable.put(line,v);
                                defaultVertexCounter++;
                        }
                        else if(line.startsWith(Constants.defaultVertex) && foundResVertices) {
                        	Point p = new Point(lines.get(i+1));
				//System.out.println("NEWTEST::::"+lines.get(i+1)+":::");
                                Object v = graph.insertVertex(parent, Constants.DEFAULT_STATE, line, p.getX(), p.getY(), Constants.DEFAULT_VERTEX_WIDTH, Constants.DEFAULT_VERTEX_HEIGHT, Constants.DEFAULT_VERTEX);
                                vertexTable.put(line,v);
                                defaultVertexCounter++;
                      	}
                        i++;
  		}

                int j=0;

                boolean foundResEdges = false;
                for(String line : lines) {
                	if(line.startsWith(Constants.START_OF_RESOURCE_VERTICES_IN_FILE+" "+(resCnt+1))) {
                        	break;
                        }
                        if(line.startsWith(Constants.START_OF_RESOURCE_EDGES_IN_FILE+" "+resCnt)) {
                                foundResEdges = true;
                        }
                        if(foundResEdges && line.equals(Constants.START_OF_RESOURCE_EDGE_IN_FILE)) {
                        	graph.insertEdge(parent, null, lines.get(j+2), vertexTable.get(lines.get(j+1)), vertexTable.get(lines.get(j+3)));
                        }
                        j++;
                }

                //graphComponent.zoomIn();
                //graphComponent.zoomOut();
                //graphComponent.zoomIn();
		
	}

	public JFrame getButtons() {
		return buttons;
	}

	public Resource(JMenuItem createTop, JMenuItem displayTop, JMenuItem displayCont, JMenuItem displayOpSched, JMenuItem checkMan, JLabel manButton)
	{
		super(Constants.TITLE);

                JLabel syntaxErrButton = new JLabel("Syntax Error");
		syntaxErrButton.setOpaque(true);
		syntaxErrButton.setVisible(true);
                Border border = BorderFactory.createRaisedSoftBevelBorder();
                syntaxErrButton.setBorder(border);

                buttons = new JFrame();

                JLabel infoPanelLabel = new JLabel(Constants.ALERTS_GUI_LABEL);
                buttons.add(infoPanelLabel);

                buttons.setLayout(new FlowLayout());
                buttons.add(syntaxErrButton);
                this.setLayout(new BorderLayout());
	        this.add(buttons.getContentPane(),BorderLayout.SOUTH);
	
		createTopologyMenuItem = createTop;
		displayTopMenuItem = displayTop;
		displayContMenuItem = displayCont;
		displayOpSchedMenuItem = displayOpSched;
		checkManMenuItem = checkMan;	
		manufButton = manButton;	
		//syntaxErrorMenuItem = syntaxErrB;	
		//syntaxError = syntaxErr;
		
		graph = new mxGraph();

                //Map<String, Object> style = graph.getStylesheet().getDefaultEdgeStyle();
                //style.put(mxConstants.STYLE_ROUNDED, true);
                //style.put(mxConstants.STYLE_EDGE, mxConstants.EDGESTYLE_ENTITY_RELATION);

		parent = graph.getDefaultParent();

		graph.setStylesheet(Helper.createStylesheet(graph));
		graph.setAllowLoops(true);
		graph.setAutoSizeCells(true);
		graph.getModel().beginUpdate();

		try {}
		finally { graph.getModel().endUpdate(); }

                graph.setAllowDanglingEdges(false);
                //graph.setDefaultOverlap(0.9);
		graph.setEdgeLabelsMovable(false);


		//final mxGraphComponent 
		graphComponent = new mxGraphComponent(graph);
                //graphComponent.getViewport().setOpaque(true);
                //graphComponent.getViewport().setBackground(Color.WHITE);

                ((JComponent)graphComponent).setBorder(BorderFactory.createEmptyBorder());

                //graphComponent.zoomIn();
                //graphComponent.zoomOut();
                //graphComponent.zoomIn();


                // Installs automatic validation (use editor.validation = true
                // if you are using an mxEditor instance)
                graph.getModel().addListener(mxEvent.CHANGE, new mxIEventListener() {
                	public void invoke(Object sender, mxEventObject evt) {	

				autoArrange();

				createTopologyMenuItem.setEnabled(false);
				displayTopMenuItem.setEnabled(false);
				displayContMenuItem.setEnabled(false);
				displayOpSchedMenuItem.setEnabled(false);
				checkManMenuItem.setEnabled(false);
				manufButton.setBackground(null);

				//java.util.List lst = graph.findTreeRoots(parent);
				if(parent != null) {
					Object [] arr = new Object[1];
					arr[0] = parent; 
					Object[] cells = graph.getCellsForGroup(arr);
					cells = graph.getAllEdges(cells);
				
					syntaxError = false;
					for( int i = 0; i <= cells.length-1; i++) {
						mxCell newCell = (mxCell)cells[i];
						if(newCell.getValue().equals(Constants.defaultEdge) ||
								newCell.getValue().equals("")) {
							newCell.setValue(Constants.defaultEdge);
							//newCell.setStyle("fontColor=#ff0000;gradientColor=#ff0000;shadow=0");	
							newCell.setStyle(Constants.NOP_EDGE);
						}
						
						else if (newCell.getValue().toString().startsWith(Constants.UNOBSERVABLE_PREFIX)) {
							newCell.setStyle(Constants.UNOBSERVABLE_EDGE);
						}
						
						
						else if(newCell.getValue().toString().startsWith(Constants.INSYNC) ||
								newCell.getValue().toString().startsWith(Constants.OUTSYNC)) {

							newCell.setStyle(Constants.IN_OUT_EDGE);	
							//if(!syntaxError) {
							//	syntaxError = false;
							//}
						}
						else if(!((String)newCell.getValue()).matches(Constants.OP_SYNTAX_REGEX)) {    
							newCell.setStyle(Constants.EDGE_SYNTAX_ERROR);
							syntaxError = true;
						}

						else {
							newCell.setStyle(Constants.DEFAULT_EDGE);	
							//if(!syntaxError) {
							//	syntaxError = false;
							//}
						}
					}
					if(syntaxError) {
						syntaxErrButton.setBackground(Constants.SYNTAX_ERR_COLOR);
					}
					else {
						syntaxErrButton.setBackground(null);
					}

					//saveCurrentResource(cells);
					graph.refresh();
    				}
				graphComponent.validateGraph();
                	}
                });
                                                                                                                                                                   // Initial validation
                graphComponent.validateGraph();
		
                new mxRubberband(graphComponent);
		getContentPane().add(graphComponent);
		graphComponent.getGraphControl().setFocusable(true);

		graphComponent.getGraphControl().addMouseListener(new MouseAdapter()
		{
			Object v;	
			public void mouseReleased(MouseEvent e) {
				mxCell cell = (mxCell)graphComponent.getCellAt(e.getX(), e.getY());
				//System.out.println(cell.getStyle());	
				
                                if (SwingUtilities.isRightMouseButton(e)) {
                                        SwingUtilities.invokeLater(new Runnable() {
                                                public void run() {

                                                        String edgeName = graph.getLabel(cell);
                                                        //StringTokenizer toks = new StringTokenizer(edgeName,";|");
                                                        //boolean goNext = true;
                                                        //while(toks.hasMoreTokens() && goNext) {
                                                                //String edgeSym = Only(toks.nextToken().trim());
                                                                OpRequestForm req = new OpRequestForm();
                                                           	req.createAndShowGui(null, edgeName);
                                                         //       goNext = req.createAndShowGui(null, edgeName);
                                                        //}
                                                }
                                        });
                                }
				else if (cell != null && 
				    e.getClickCount() == 2 &&
				    cell.isEdge()) {
					System.out.println("cell="+graph.getLabel(cell));
					
				}
                                else if (cell == null && e.getClickCount() == 2) {


                                        if(defaultVertexCounter == 0) {
                                                Object v = graph.insertVertex(parent, Constants.INITIAL_STATE, Constants.defaultVertex+defaultVertexCounter, e.getX(), e.getY(), Constants.DEFAULT_VERTEX_WIDTH, Constants.DEFAULT_VERTEX_HEIGHT, Constants.INITIAL_VERTEX);
						//((mxCell)v).setId(Constants.INITIAL_STATE);
                                        }
                                        else {
                                                Object v = graph.insertVertex(parent, Constants.DEFAULT_STATE, Constants.defaultVertex+defaultVertexCounter, e.getX(), e.getY(), Constants.DEFAULT_VERTEX_WIDTH, Constants.DEFAULT_VERTEX_HEIGHT, Constants.DEFAULT_VERTEX);
						//((mxCell)v).setIdValue(Constants.DEFAULT_STATE);
                                        }

		   			//graph.insertVertex(parent, null, Constants.defaultVertex+defaultVertexCounter, e.getX(), e.getY(), 80, 30, Constants.DEFAULT_VERTEX);
					defaultVertexCounter++;			
					graphComponent.scrollCellToVisible(cell);
				}
				if (cell != null && e.getClickCount() == 1 && timer != null) {
					long timeClicked = new Date().getTime() - timer.getTime();
					if (timeClicked >= Constants.DELETE_CELL_TIME) {
						timer = null;
				    		graph.removeCells(); 
					}
				}
			}

                	public void mousePressed(MouseEvent e) {
				timer = new Date();
                	}
		});
	}


        public void clipLabels(boolean clip) {
                keepGraphsClipped = clip;
                clipLabels();
        }

        public void clipLabels() {
                graph.setLabelsClipped(keepGraphsClipped);
                graphComponent.refresh();
        }


        public void autoArrange(boolean arrange) {
                keepResourceAutoArranged = arrange;
		autoArrange();
	}

        public void autoArrange() {

                if(keepResourceAutoArranged) {
                        mxHierarchicalLayout layout = new mxHierarchicalLayout(graph,SwingConstants.WEST);
                        layout.setFineTuning(true);
                        layout.setInterRankCellSpacing(200);
                        layout.setIntraCellSpacing(100);
			try {
                        	layout.execute(parent);
			}
			catch(Exception e) {
				System.out.println("Could not apply layout to this resource");
			}
                }
                else {
			mxParallelEdgeLayout layout = new mxParallelEdgeLayout(graph, Constants.PARALLEL_EDGE_OFFSET);
			layout.execute(parent);
                }
                clipLabels();
        }



   	public void saveCurrentResource() {

		Object [] arr = new Object[1];
                arr[0] = parent;
                Object[] cells = graph.getCellsForGroup(arr);
                cells = graph.getAllEdges(cells);

        	vertices = new HashSet<String>();
        	labels = new HashSet<String>();
        	edges = new HashSet<ResourceTransition>();

		for( int i = 0; i < cells.length; i++) {
                	mxCell newCell = (mxCell)cells[i];
			//System.out.println((newCell.getSource()).getValue());
			//System.out.println(newCell.getValue());
			//System.out.println((newCell.getTarget()).getValue()+"\n");

			String vertex1 = (newCell.getSource()).getValue().toString();
			String vertex2 = (newCell.getTarget()).getValue().toString();
			ResourceTransition rt = new ResourceTransition(vertex1, newCell.getValue().toString(), vertex2);
			if(!Helper.isIn(rt,edges,Constants.RESOURCE))
				edges.add(rt);
			if(!Helper.isIn(vertex1,vertices))
				vertices.add(vertex1);
			if(!Helper.isIn(vertex2,vertices))
				vertices.add(vertex2);
			if(!Helper.isIn(newCell.getValue().toString(),labels))
				labels.add(newCell.getValue().toString());
		}
	}



   	public Set getVertices() {
		return vertices;	
	}



   	public Set getEdges() {
		return edges;
	}



        public Set getLabels() {
                return labels;
        }


        public mxGraph getGraph() {
                return graph;
	}

	public void zoomIn() {
        	graphComponent.zoomIn();
	}

	public void zoomOut() {
        	graphComponent.zoomOut();
	}

   	private String readFile(String path) throws IOException { 
		StringBuilder sb = new StringBuilder();
    		try (BufferedReader br = new BufferedReader(new FileReader(path))) {
			String sCurrentLine;
        		while ((sCurrentLine = br.readLine()) != null) {
            			sb.append(sCurrentLine);
        		}
    		}
          	return sb.toString();
    	}


	/**
        public Point getXY(String vertex) {

		return Helper.getXY(vertex, graph);
        }
	**/


        public Point getXY(String vertex) {

                Object [] arr = new Object[1];
                arr[0] = parent;
                Object[] cells = graph.getCellsForGroup(arr);
                cells = graph.getAllEdges(cells);

                //System.out.println("VERTEX:"+vertex);
                for(int i = 0; i <= cells.length-1; i++) {
                        mxCell newCell = (mxCell)cells[i];
                        String vertex1 = (newCell.getSource()).getValue().toString();
                        String vertex2 = (newCell.getTarget()).getValue().toString();
                        //System.out.println("CELL:"+vertex1+",CELL:"+vertex2);

                        if(vertex1.equals(vertex)) {
                                return new Point((newCell.getSource()).getGeometry().toString());
                        }
                        else if(vertex2.equals(vertex)) {
                                return new Point((newCell.getTarget()).getGeometry().toString());
                        }
                }

                return null;
        }


}
