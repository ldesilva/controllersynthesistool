package controllerSynth;
import java.util.*;

public class OperationExpression {

	public String label;
	private StringTokenizer labToks;
	private int nextExprNum = 1;


        public OperationExpression(String l) {
        	label = l;
		//labToks = new StringTokenizer(label, Constants.SEPARATOR);
		//labToksPrev = new StringTokenizer(label, Constants.SEPARATOR);
        }


        OperationExpression(OperationExpression exp) {
		label = new String(exp.label);
		nextExprNum = exp.nextExprNum;
	}


	public String nextExpr() {
		labToks = new StringTokenizer(label, Constants.SEPARATOR);

		for(int i=1; i<nextExprNum; i++) {	
			labToks.nextToken();	
		}

		if(!labToks.hasMoreTokens()) {
			return null;
		}

		nextExprNum++;
		return (String)labToks.nextToken().trim();
	}


	public void undoOneStep() {
		nextExprNum--;
	}


	public boolean isEmpty() {
		labToks = new StringTokenizer(label, Constants.SEPARATOR);
		return (nextExprNum > labToks.countTokens());
		//return !labToks.hasMoreTokens();
	}
}

