package controllerSynth;
import java.util.*;

public class SimulationTriple {

	public List<Object> topologyState;
        public String recipeState;
        public ResourceVector resourceVector;

        SimulationTriple(List<Object> a, String b, ResourceVector rv) {
        	this.topologyState = a;
                this.recipeState = b;
                this.resourceVector = rv;
        }

	public List<String> getRVasStr(int resCnt) {

		List<String> rv = new ArrayList<String>();

		for(int i=0; i<resCnt; i++) {
			rv.add(resourceVector.rv[i].toString());
		} 
		
		return rv;
	}

	/** 
 	 * Printing out for debugging purposes
 	 **/
	//@Override
	public String toString(int resCnt) {
		String out = new String();
		out = "[";
		int i=0;
        	for (Object s : topologyState) {
			i++;
			if(i < resCnt)
				out += s.toString()+", ";
			else
				out += s.toString();
		}
		//out += ">, \n";
		out += "]\n";
		out += recipeState;
		//out += ",\n <";
		out += "\n";
		for(i=0; i < resCnt; i++) {
			if(i < resCnt-1)
				out += (resourceVector.rv[i]).toString()+",";
			else
				out += (resourceVector.rv[i]).toString();
		}
		//out += ">";

		return out;
	}
}

