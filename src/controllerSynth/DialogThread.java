package controllerSynth;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.*;
import java.awt.*;
import javax.swing.event.*;
import java.awt.event.*;
import javax.swing.text.BadLocationException;
import javax.swing.border.Border;

public class DialogThread implements Runnable {

	private JFrame frame;
	private JDialog dialog;
	private boolean stopThread = false; 
	private	JOptionPane optionPane;
	private long start, end;
	private String padding = "   ";
	private String time = "";
	private String message;

	public void run() {

		new Thread() {
			public void run() {

				start = System.currentTimeMillis();

				JOptionPane optionPane = new JOptionPane(message+padding,
                		JOptionPane.INFORMATION_MESSAGE,
                        	JOptionPane.DEFAULT_OPTION,
                        	null,
                        	new Object[]{},
                        	null);

				dialog = new JDialog(frame);
                		dialog.setTitle("Message");
                		dialog.setModal(true);
                		dialog.setContentPane(optionPane);
                		dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
                		dialog.pack();
				dialog.setLocationRelativeTo(frame);
                		dialog.setVisible(true);
			}
		}.start();

		new Thread() {
			public void run() {
				String dots = "";
				long count = 0;

				//NumberFormat formatter = new DecimalFormat("#0.00000");

				while(!stopThread) {
				
					end = System.currentTimeMillis();

					long seconds = (end - start) / 1000;
					long minutes = seconds / 60;
					seconds = seconds % 60;
					long hours = minutes / 60;
					minutes = minutes % 60;
					long days = hours / 24;
					hours = hours % 24;
					
					//formatter.format((end - start) / 1000d)

					time = "";

					if(days > 0) {
						time += Long.toString(days) + "d:";
					}
					if(hours > 0) {
						time += Long.toString(hours) + "h:";
					}
					if(minutes > 0) {
						time += Long.toString(minutes) + "m:";
					}
					if(seconds > 0) {
						time += Long.toString(seconds) + "s";
					}
					else {
						time += "0s";
						//time += (end - start)+"ms";
					}

					count++;
					try { Thread.sleep(1000); } catch(Exception e) {};
					optionPane = new JOptionPane(message+" ["+time+"]"+padding,
                        					     JOptionPane.INFORMATION_MESSAGE,
                        					     JOptionPane.DEFAULT_OPTION,
                        					     null,
                        					     new Object[]{},
                        					     null);

					//dialog = new JDialog(frame);
                			dialog.setTitle("Message");
                			dialog.setModal(true);
                			dialog.setContentPane(optionPane);
                			dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
                			dialog.pack();
					dialog.setLocationRelativeTo(frame);
                			dialog.setVisible(true);

					dots += ".";
					if(count % 4 == 0) {
						dots = "";
					}
				}
				dialog.dispose();
			}
		}.start();
    	}

	DialogThread(JFrame frame, String message) {

		this.frame = frame;
		this.message = message;
	}

	public String getTime() {
		if(time.equals("0s")) {
			return Long.toString(end - start) + "ms";
		}
		return time;
	}

	public void interrupt() {
		stopThread = true;
	}

    	//public static void main(String args[]) {
        	//(new HelloThread()).start();
    	//}

}
