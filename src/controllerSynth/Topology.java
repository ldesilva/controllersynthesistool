package controllerSynth;
//package com.mxgraph.examples.swing;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.event.*;
import javax.swing.text.BadLocationException;
//import java.awt.event.ActionListener;
//import java.awt.event.ActionEvent;
import java.awt.*;
import java.io.*;
//import java.awt.event.FocusEvent;
//import java.awt.event.FocusListener;
import java.util.*;

import com.mxgraph.swing.handler.mxRubberband;
import com.mxgraph.util.mxEvent;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxEventObject;
import com.mxgraph.util.mxEventSource.mxIEventListener;

import javax.swing.JFrame;

import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.*;
import com.mxgraph.model.*;
import com.mxgraph.layout.hierarchical.*;




public class Topology extends JFrame
{
	
	/**
	 * 
	 */
	//private static final long serialVersionUID = -2764911804288120883L;
	//private static final String circle = "shape=ellipse;perimeter=ellipsePerimeter";
	//private static final String B2MMLfolder = "B2MML/";
	//private static final String defaultVertex = "State";
	////private static final String NOP = "nop";
	//private static final String defaultEdge = Constants.NOP; 
	//private static final String TITLE = "Work Order Development Toolkit"; 
	//private static String b2mmlViewer = "B2MML Viewer"; 
	//private static final String INSYNC = "in:"; 
	//private static final String OUTSYNC = "out:"; 
	//private static final String IN_OUT_COLOR = "fontColor=#DC143C"; 
	//private static final String NOP_COLOR = "fontColor=#00C5CD"; 
	//private static final String DEFAULT_COLOR = "fontColor=#000000"; 
	
	final mxGraphComponent graphComponent;
	private mxHierarchicalLayout layout;
	private int defaultEdgeCounter = 0;
	private long vertices = 0;
	private long edges = 0;
	private int defaultVertexCounter = 0; 
        private java.util.Date timer;
 	private Object parent = null;
	//private Set<String> vertices = new HashSet<String>();
	//private Set<String> labels = new HashSet<String>();
	private Set<TopologyTransition> topEdges = new HashSet<TopologyTransition>();
	private mxGraph graph;
	private int resourceCount;
	private int yCoord = 20;
        private JLabel manufButton;
        private JFrame buttons;
        private boolean keepGraphsClipped = false;


	public Topology(Set<TopologyTransition> edges, JLabel manButton, int resCount)
	{
		super(Constants.TITLE);

                buttons = new JFrame();

                JLabel infoPanelLabel = new JLabel(Constants.ALERTS_GUI_LABEL);
                buttons.add(infoPanelLabel);

                buttons.setLayout(new FlowLayout());
                this.setLayout(new BorderLayout());
                this.add(buttons.getContentPane(),BorderLayout.SOUTH);

                manufButton = manButton;
		topEdges = edges;
		resourceCount = resCount;

		graph = new mxGraph();

    		Map<String, Object> style = graph.getStylesheet().getDefaultEdgeStyle();
    		style.put(mxConstants.STYLE_ROUNDED, true);
    		style.put(mxConstants.STYLE_EDGE, mxConstants.EDGESTYLE_ENTITY_RELATION);

		parent = graph.getDefaultParent();

		graph.setStylesheet(Helper.createStylesheet(graph));
		graph.setAllowLoops(true);
		graph.getModel().beginUpdate();

                try
                {
                	java.util.List<Object> topState = new ArrayList<Object>();
                        for(int i=0; i<resourceCount; i++) { 
				topState.add("state0");
       			}

			int x = 20;
			//int y = 20;
			Set<Object> visited = new HashSet<Object>();

                        Object v1 = graph.insertVertex(parent, 
                                                       null, 
                                                       //topState.toString().replaceAll("state", "s"), 
                                                       topState.toString(), 
                                                       x,     
                                                       yCoord,     
						       topState.toString().length() * Constants.TOPOLOGY_LABEL_SIZE_RATIO * Constants.TOPOLOGY_VERTEX_FONTSIZE,
                                                       //topState.toString().length() * Constants.WIDTH_PER_CHAR,    
                                                       Constants.TOPOLOGY_VERTEX_HEIGHT,    
                                                       Constants.TOPOLOGY_VERTEX_STYLE);
			visited.add(v1);
			drawTop(v1, x, visited);
                }

		finally { graph.getModel().endUpdate(); }

                layout = new mxHierarchicalLayout(graph,SwingConstants.WEST);
                layout.setFineTuning(true);
                layout.setInterRankCellSpacing(200);
                layout.setIntraCellSpacing(100);
                layout.execute(parent);

                graph.setAllowDanglingEdges(false);
                //graph.setDefaultOverlap(0.9);
		graph.setEdgeLabelsMovable(false);


		graphComponent = new mxGraphComponent(graph);
		clipLabels();

                graph.getModel().addListener(mxEvent.CHANGE, new mxIEventListener() {
                	public void invoke(Object sender, mxEventObject evt) {	
                                layout = new mxHierarchicalLayout(graph,SwingConstants.WEST);
                                layout.setFineTuning(true);
                                layout.setInterRankCellSpacing(200);
                                layout.setIntraCellSpacing(100);
                                layout.execute(parent);

				clipLabels();

                	}
                });

                // Initial validation
                graphComponent.validateGraph();
		
                new mxRubberband(graphComponent);
		getContentPane().add(graphComponent);
		graphComponent.getGraphControl().setFocusable(true);

		graphComponent.getGraphControl().addMouseListener(new MouseAdapter()
		{
			Object v;	
			public void mouseReleased(MouseEvent e) { }

		});
	}


        public JFrame getButtons() {
                return buttons;
        }


        public void zoomIn() {
                graphComponent.zoomIn();
        }

        public void zoomOut() {
                graphComponent.zoomOut();
        }


        public void clipLabels(boolean clip) {
		keepGraphsClipped = clip;
		clipLabels();
	} 

        public void clipLabels() {
                graph.setLabelsClipped(keepGraphsClipped);
                graphComponent.refresh();
        }


	/**
        public void countCells() {
		Object [] arr = new Object[1];
                arr[0] = parent;
                Object[] cells = graph.getCellsForGroup(arr);
                cells = graph.getAllEdges(cells);
		vertices = 0;
		edges = 0;

		for( int i = 0; i <= cells.length-1; i++) {
                	mxCell newCell = (mxCell)cells[i];	
			if(newCell.isVertex())
				vertices++;
			else if(newCell.isEdge())
				edges++;
		}
	}

        public long getNoOfVertices() {
		return vertices;
        }

        public long getNoOfEdges() {
		return edges;
	}
	**/

        private void drawTop(Object v1, int x, Set<Object> visited) {

		//x += 300;
		//x += topEdge.label.toString().length() * Constants.TOPOLOGY_LABEL_SIZE_RATIO * Constants.TOPOLOGY_EDGE_FONTSIZE;
		double newX = 0;

		boolean firstTimeInLoop = true;

                for (TopologyTransition topEdge : topEdges) {
			//String src = topEdge.source.toString().replaceAll("state", "s");
			String src = topEdge.source.toString();
                        if(src.equals(graph.getLabel(v1))) {

				boolean alreadyVisitedv2 = Helper.vtxIsIn(topEdge.target.toString(), visited);
				Object v2;

				if(!alreadyVisitedv2) {
				
					newX = x + topEdge.label.toString().length() * Constants.TOPOLOGY_EDGE_RATIO * Constants.TOPOLOGY_EDGE_FONTSIZE;

                                	if(!firstTimeInLoop) {
                                        	yCoord += Constants.TOPOLOGY_Y_OFFSET;
                                	}

                                	firstTimeInLoop = false;

                        		v2 = graph.insertVertex(parent, 
							       null, 
							       //topEdge.target.toString().replaceAll("state", "s"), 
							       topEdge.target.toString(), 
							       (int)newX, 
							       yCoord, 
							       topEdge.target.toString().length() * Constants.TOPOLOGY_LABEL_SIZE_RATIO * Constants.TOPOLOGY_VERTEX_FONTSIZE, 
							       Constants.TOPOLOGY_VERTEX_HEIGHT, 
							       Constants.TOPOLOGY_VERTEX_STYLE);
				}
				else {
					v2 = Helper.getVertex(topEdge.target.toString(), visited);
				}
				
				
                        	graph.insertEdge(parent, null, topEdge.label.toString(), v1, v2, Constants.TOPOLOGY_EDGE_STYLE);

				if(!alreadyVisitedv2) {
					visited.add(v2);		
					drawTop(v2, (int)newX, visited);
				}

				//Point p = new Point();
				//p.setX(x);
				//p.setY(y);
				//points.add(p);
                        }
                }       
		//yCoord -= 100;
        }
}
