package controllerSynth;
import java.util.*;
import javax.swing.*;
import com.mxgraph.view.*;
import com.mxgraph.util.*;
import com.mxgraph.model.mxCell;
import java.io.*;
import javax.swing.filechooser.FileNameExtensionFilter;



public class Helper {

        public static boolean isIn(List<Object> listOfVertices, Set<List<Object>> visitedVertices) {

               	for (List<Object> vertices : visitedVertices) {

			int vtxCount = 0;
			boolean found = true;
               		for (Object vertex : vertices) {

				if(!((String)vertex).equals((String)listOfVertices.get(vtxCount))) {
					found = false;
					break;
				}

				vtxCount++;
			}
			if(found) {
				return true;
			}
		}

		return false;
        }





        public static boolean isIn(Object rt, Set edges, String type) {

                for (Object edge : edges) {
			if(type.equals(Constants.RESOURCE)) {
				ResourceTransition rt1 = (ResourceTransition) rt;
                        	ResourceTransition rt2 = (ResourceTransition)edge;
                        	if(rt1.source.equals(rt2.source) &&
                           	   rt1.label.equals(rt2.label) &&
                           	   rt1.target.equals(rt2.target))
            			return true;
			}
			else if(type.equals(Constants.RECIPE)) {
                        	RecipeTransition rt1 = (RecipeTransition) rt;
                        	RecipeTransition rt2 = (RecipeTransition)edge;

                        	if(rt1.source.equals(rt2.source) &&
 			  	   rt1.label.label.equals(rt2.label.label) &&
                                   rt1.target.equals(rt2.target))
                                return true;
			}
			else {
				TopologyTransition rt1 = (TopologyTransition) rt;
                        	TopologyTransition rt2 = (TopologyTransition)edge;
                        	if(rt1.source.toString().trim().equals(rt2.source.toString().trim()) &&
                           	   rt1.label.toString().trim().equals(rt2.label.toString().trim()) &&
                           	   rt1.target.toString().trim().equals(rt2.target.toString().trim()))
            			return true;
			}	
                }

                return false;
        }



        public static boolean isIn(String sourceStr, Set strings) {
		
                for (Object targetStr : strings) {
			if(((String)targetStr).equals(sourceStr)) {
            			return true;
			}
		}

                return false;
	}



        public static boolean contains(Set<Sigma> visited, Sigma item, int resCount) {

                for (Sigma targetItem : visited) {
			if(targetItem.topologyState.toString().equals(item.topologyState.toString()) &&
			   targetItem.resourceVector.getAsString(resCount).equals(item.resourceVector.getAsString(resCount))) {
				
				return true;	
			}
                }

                return false;
        }


        public static Set<Sigma> clone(Set<Sigma> set) {

		Set<Sigma> newSet = new HashSet<Sigma>();

                for (Sigma sigma : set) {

			List<Object> newTopologyState = new ArrayList<Object>();

                	for (Object obj : sigma.topologyState) {
				newTopologyState.add(obj.toString());
			}

			ResourceVector newRV = new ResourceVector(sigma.resourceVector);
					
			Sigma newSigma = new Sigma(newTopologyState, newRV);
			newSet.add(newSigma);	
		}

		return newSet;
        }



        public static boolean vtxIsIn(String sourceStr, Set vertices) {
		//System.out.println("");
                for (Object targetVtx : vertices) {
			//System.out.println("SOURCE:"+sourceStr+"; TARGET:"+((mxCell)targetVtx).getValue().toString());
                        if(((mxCell)targetVtx).getValue().toString().equals(sourceStr)) {
			//	System.out.println("TRUE");
                                return true;
                        }
                }

		//System.out.println("FALSE");
                return false;
        }


        public static Object getVertex(String sourceStr, Set vertices) {

                for (Object targetVtx : vertices) {
                        if(((mxCell)targetVtx).getValue().toString().equals(sourceStr)) {
                                return targetVtx;
                        }
                }

                return null;
        }



        public static mxStylesheet createStylesheet(mxGraph graph) {

                mxStylesheet stylesheet = graph.getStylesheet();

                Hashtable<String, Object> style = new Hashtable<String, Object>();
                style.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_ELLIPSE);
                style.put(mxConstants.STYLE_OPACITY, 50);
                style.put(mxConstants.STYLE_AUTOSIZE, 1);
                style.put(mxConstants.STYLE_FONTCOLOR, "#000000");
                style.put(mxConstants.STYLE_FONTSIZE, Constants.VERTEX_FONTSIZE);
                style.put(mxConstants.STYLE_FONTFAMILY, Constants.VERTEX_FONT);
                style.put(mxConstants.STYLE_STROKEWIDTH, 2.0);
                //style.put(mxConstants.STYLE_SHADOW, true);
                stylesheet.putCellStyle(Constants.DEFAULT_VERTEX, style);

                Hashtable<String, Object> topStyle = new Hashtable<String, Object>();
                topStyle.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_ELLIPSE);
                topStyle.put(mxConstants.STYLE_OPACITY, 50);
                topStyle.put(mxConstants.STYLE_AUTOSIZE, 1);
                topStyle.put(mxConstants.STYLE_FONTCOLOR, "#000000");
                topStyle.put(mxConstants.STYLE_FONTSIZE, Constants.TOPOLOGY_VERTEX_FONTSIZE);
                topStyle.put(mxConstants.STYLE_FONTFAMILY, Constants.VERTEX_FONT);
                topStyle.put(mxConstants.STYLE_STROKEWIDTH, 2.0);
                //topStyle.put(mxConstants.STYLE_SHADOW, true);
                stylesheet.putCellStyle(Constants.TOPOLOGY_VERTEX_STYLE, topStyle);


                Hashtable<String, Object> styleInitVtx = new Hashtable<String, Object>();
                styleInitVtx.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_DOUBLE_ELLIPSE);
                styleInitVtx.put(mxConstants.STYLE_OPACITY, 50);
                styleInitVtx.put(mxConstants.STYLE_FONTCOLOR, "#000000");
                styleInitVtx.put(mxConstants.STYLE_FONTSIZE, Constants.VERTEX_FONTSIZE);
                //styleInitVtx.put(mxConstants.STYLE_AUTOSIZE, 1);
                styleInitVtx.put(mxConstants.STYLE_FONTFAMILY, Constants.VERTEX_FONT);
                //styleInitVtx.put(mxConstants.STYLE_FONTSTYLE, mxConstants.FONT_BOLD);
                styleInitVtx.put(mxConstants.STYLE_STROKEWIDTH, 2.0);
                styleInitVtx.put(mxConstants.STYLE_ARCSIZE, 60);
                //styleInitVtx.put(mxConstants.STYLE_SHADOW, true);
                stylesheet.putCellStyle(Constants.INITIAL_VERTEX, styleInitVtx);

                Hashtable<String, Object> styleTopEdge = new Hashtable<String, Object>();
                styleTopEdge.put(mxConstants.STYLE_FONTCOLOR, "#000000");
                styleTopEdge.put(mxConstants.STYLE_FONTSIZE, Constants.TOPOLOGY_EDGE_FONTSIZE);
                //styleTopEdge.put(mxConstants.STYLE_EDGE, mxConstants.EDGESTYLE_ORTHOGONAL);
                //styleTopEdge.put(mxConstants.STYLE_EDGE, mxConstants.EDGESTYLE_ELBOW);
                //styleTopEdge.put(mxConstants.STYLE_ELBOW, mxConstants.ELBOW_HORIZONTAL);
                //styleTopEdge.put(mxConstants.STYLE_EDGE, mxConstants.EDGESTYLE_SEGMENT);
                //styleTopEdge.put(mxConstants.STYLE_EDGE, mxConstants.EDGESTYLE_SIDETOSIDE);
                //styleTopEdge.put(mxConstants.STYLE_EDGE, mxConstants.EDGESTYLE_TOPTOBOTTOM);
                styleTopEdge.put(mxConstants.STYLE_FONTFAMILY, Constants.EDGE_FONT);
                styleTopEdge.put(mxConstants.STYLE_FILL_OPACITY, 100);
                styleTopEdge.put(mxConstants.STYLE_ENDSIZE, 10);
                styleTopEdge.put(mxConstants.STYLE_STROKEWIDTH, 1.5);
                //styleTopEdge.put(mxConstants.STYLE_SHADOW, true);
                stylesheet.putCellStyle(Constants.TOPOLOGY_EDGE_STYLE, styleTopEdge);

                Hashtable<String, Object> styleEdge = new Hashtable<String, Object>();
                //style.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_ELLIPSE);
                //style.put(mxConstants.STYLE_OPACITY, 50);
                styleEdge.put(mxConstants.STYLE_FONTCOLOR, "#000000");
                styleEdge.put(mxConstants.STYLE_FONTSIZE, Constants.EDGE_FONTSIZE);
                styleEdge.put(mxConstants.STYLE_FONTFAMILY, Constants.EDGE_FONT);
                //styleEdge.put(mxConstants.STYLE_STROKECOLOR, "#00C5CD"); // red
		//styleEdge.put(mxConstants.STYLE_OVERFLOW, "hidden");
		styleEdge.put(mxConstants.STYLE_FILL_OPACITY, 100);
                styleEdge.put(mxConstants.STYLE_ENDSIZE, 10);
                styleEdge.put(mxConstants.STYLE_STROKEWIDTH, 1.5);
                //styleEdge.put(mxConstants.STYLE_SHADOW, true);
		//styleEdge.put(mxConstants.STYLE_EDGE, mxConstants.EDGESTYLE_ORTHOGONAL);
		//styleEdge.put(mxConstants.SHAPE_CURVE, 10);
		//styleEdge.put(mxConstants.STYLE_BENDABLE, 1);
                //styleEdge.put(mxConstants.ARROW_WIDTH, "40"); 
		//styleEdge.put(mxConstants.STYLE_LABEL_BACKGROUNDCOLOR, "#ffffff");
                stylesheet.putCellStyle(Constants.DEFAULT_EDGE, styleEdge);

                Hashtable<String, Object> styleEdgeNop = new Hashtable<String, Object>();
                styleEdgeNop.put(mxConstants.STYLE_FONTSIZE, Constants.EDGE_FONTSIZE);
                styleEdgeNop.put(mxConstants.STYLE_FONTFAMILY, Constants.EDGE_FONT);
                //styleEdgeNop.put(mxConstants.STYLE_FONTCOLOR, "#00C5CD"); // red
                styleEdgeNop.put(mxConstants.STYLE_FONTCOLOR, "#DC143C"); // blue 
                //styleEdgeNop.put(mxConstants.STYLE_STROKECOLOR, "#00C5CD"); 
		//styleEdgeNop.put(mxConstants.STYLE_LABEL_BACKGROUNDCOLOR, "#ffffff");
		//styleEdgeNop.put(mxConstants.STYLE_OVERFLOW, "hidden");
		styleEdgeNop.put(mxConstants.STYLE_FILL_OPACITY, 100);
                //styleEdgeNop.put(mxConstants.ARROW_WIDTH, "40"); 
                styleEdgeNop.put(mxConstants.STYLE_ENDSIZE, 10);
                styleEdgeNop.put(mxConstants.STYLE_STROKEWIDTH, 1.5);
                //styleEdgeNop.put(mxConstants.STYLE_SHADOW, true);
		styleEdgeNop.put(mxConstants.STYLE_DASHED, true);
                stylesheet.putCellStyle(Constants.NOP_EDGE, styleEdgeNop);
                
                Hashtable<String, Object> styleUnobservableAction = new Hashtable<String, Object>();
                styleUnobservableAction.put(mxConstants.STYLE_FONTSIZE, Constants.EDGE_FONTSIZE);
                styleUnobservableAction.put(mxConstants.STYLE_FONTFAMILY, Constants.EDGE_FONT);
                styleUnobservableAction.put(mxConstants.STYLE_FONTCOLOR, "#cc3939"); // red 
                styleUnobservableAction.put(mxConstants.STYLE_FILL_OPACITY, 100);
                styleUnobservableAction.put(mxConstants.STYLE_DASHED, true);
                styleUnobservableAction.put(mxConstants.STYLE_ENDSIZE, 10);
                styleUnobservableAction.put(mxConstants.STYLE_STROKEWIDTH, 1.5);
                stylesheet.putCellStyle(Constants.UNOBSERVABLE_EDGE, styleUnobservableAction);
                
                Hashtable<String, Object> styleEdgeInOut = new Hashtable<String, Object>();
                styleEdgeInOut.put(mxConstants.STYLE_FONTSIZE, Constants.EDGE_FONTSIZE);
                styleEdgeInOut.put(mxConstants.STYLE_FONTFAMILY, Constants.EDGE_FONT);
                styleEdgeInOut.put(mxConstants.STYLE_FONTCOLOR, "#DC143C"); // blue 
		//styleEdgeInOut.put(mxConstants.STYLE_LABEL_BACKGROUNDCOLOR, "#ffffff");
		//styleEdgeInOut.put(mxConstants.STYLE_OVERFLOW, "hidden");
		styleEdgeInOut.put(mxConstants.STYLE_FILL_OPACITY, 100);
		styleEdgeInOut.put(mxConstants.STYLE_DASHED, true);
                styleEdgeInOut.put(mxConstants.STYLE_ENDSIZE, 10);
                styleEdgeInOut.put(mxConstants.STYLE_STROKEWIDTH, 1.5);
                //styleEdgeInOut.put(mxConstants.STYLE_SHADOW, true);
                //styleEdgeInOut.put(mxConstants.ARROW_WIDTH, "40"); 
                //styleEdgeInOut.put(mxConstants.STYLE_STROKECOLOR, "#00C5CD"); 
                stylesheet.putCellStyle(Constants.IN_OUT_EDGE, styleEdgeInOut);

                Hashtable<String, Object> styleEdgeErr = new Hashtable<String, Object>();
                styleEdgeErr.put(mxConstants.STYLE_FONTSIZE, Constants.EDGE_FONTSIZE);
                styleEdgeErr.put(mxConstants.STYLE_FONTFAMILY, Constants.EDGE_FONT);
                styleEdgeErr.put(mxConstants.STYLE_FONTCOLOR, "#000000");
                styleEdgeErr.put(mxConstants.STYLE_FILL_OPACITY, 100);
                styleEdgeErr.put(mxConstants.STYLE_ENDSIZE, 10);
                styleEdgeErr.put(mxConstants.STYLE_STROKEWIDTH, 1.5);
                //styleEdgeErr.put(mxConstants.STYLE_SHADOW, true);
                //styleEdgeInOut.put(mxConstants.STYLE_DASHED, true);
                styleEdgeErr.put(mxConstants.STYLE_FONTSTYLE, mxConstants.FONT_ITALIC);
                stylesheet.putCellStyle(Constants.EDGE_SYNTAX_ERROR, styleEdgeErr);


                return stylesheet;
        }



        public static void setUIFont (javax.swing.plaf.FontUIResource f) {
                java.util.Enumeration keys = UIManager.getLookAndFeelDefaults().keys();
                while (keys.hasMoreElements()) {
                        Object key = keys.nextElement();
                        Object value = UIManager.get (key);
                        if (value != null && value instanceof javax.swing.plaf.FontUIResource) {
                                UIManager.put (key, f);
                        }
                }
        }



        public static List<Object> getAtomicExprAsList(String atomicExpr) {

		StringTokenizer tokens = new StringTokenizer(atomicExpr, Constants.PARALLEL_SYMBOL);
		List<Object> atomicExprList = new ArrayList<Object>();

		while(tokens.hasMoreTokens()) {
			atomicExprList.add(tokens.nextToken().trim());
		}

		return atomicExprList;
	}



        public static List<List<Object>> clone(List<List<Object>> listlist) {

		List<List<Object>> newlistlist = new ArrayList<List<Object>>();

		for (List<Object> list : listlist) {
			
			List<Object> newlist = new ArrayList<Object>();

			for (Object obj : list) {
				String objStr = (String)obj;
				newlist.add(objStr);
			}
			newlistlist.add(newlist);
		}

		return newlistlist;
	}




        public static Set<List<Object>> cloneSet(Set<List<Object>> setlist) {

                Set<List<Object>> newsetlist = new HashSet<List<Object>>();

                for (List<Object> list : setlist) {

                        List<Object> newlist = new ArrayList<Object>();

                        for (Object obj : list) {
                                String objStr = (String)obj;
                                newlist.add(objStr);
                        }
                        newsetlist.add(newlist);
                }

                return newsetlist;
        }




        public static List<ResourceTransition> cloneEdges(List<ResourceTransition> list) {

                List<ResourceTransition> newlist = new ArrayList<ResourceTransition>();

                for (ResourceTransition trans : list) {
                	//ResourceTransition trans = (ResourceTransition)obj;
			ResourceTransition trans2 = new ResourceTransition(trans.source, trans.label, trans.target);
                        newlist.add(trans2);
                }

                return newlist;
        }





        public static boolean consistentEdges(List<ResourceTransition> edges) {

                for (ResourceTransition trans1 : edges) {

                       	//ResourceTransition trans1 = (ResourceTransition)objTrans1;

                        if(isSyncTask(trans1.label)) {

				boolean	foundMatchingSyncs = false;
                		for (ResourceTransition trans2 : edges) {
                        		//ResourceTransition trans2 = (ResourceTransition)objTrans2;

                                	if(isSyncTask(trans2.label) && 
				   	   oppositeSyncs(trans1.label,trans2.label) && 
				   	   (number(trans1.label) == number(trans2.label))) {
			
						if(!foundMatchingSyncs) 	
							foundMatchingSyncs = true;				
						else
							return false; // too many matches, e.g. [out:1, in:1, in:1] 
					}
				}

				if(!foundMatchingSyncs) {
					return false;
				}
			}
                }

                return true;
        }



        public static String removeWhiteSpaces(String xml) {

                String startXML = Constants.startXML;
                xml = xml.substring(xml.indexOf("<"+Constants.OP_REQ_XML+">"));
                xml= xml.replaceAll("\\>\\s+\\<","><");
                return startXML+xml;
        }




        public static String readFile(String path) throws IOException {
                StringBuilder sb = new StringBuilder();
                try (BufferedReader br = new BufferedReader(new FileReader(path))) {
                        String sCurrentLine;
                        while ((sCurrentLine = br.readLine()) != null) {
                                sb.append(sCurrentLine);
                        }
                }
                return sb.toString();
        }





        public static String reverse(String edge) {
                String sReversed = "";
                StringTokenizer st = new StringTokenizer(edge,";|");
                while (st.hasMoreTokens()) {
                        if(sReversed.equals("")) {
                                sReversed = st.nextToken();
                        }
                        else {
                                sReversed = st.nextToken() + " ; " + sReversed;
                        }
                }

                return sReversed;
        }


        public static int number(Object label) {

                if(!Helper.isSyncTask(label))
                        return -1;

                if(label.toString().indexOf(Constants.INSYNC) != -1) {
                        return Integer.parseInt(label.toString().substring(Constants.INSYNC.length()));
                }
                else if(label.toString().indexOf(Constants.OUTSYNC) != -1) {
                        return Integer.parseInt(label.toString().substring(Constants.OUTSYNC.length()));
                }

                return -1;
        }



        public static boolean oppositeSyncs(Object label1, Object label2) {


                if(label1.toString().indexOf(Constants.INSYNC) != -1 &&
                   label2.toString().indexOf(Constants.OUTSYNC) != -1) {
                        return true;
                }
                else if(label2.toString().indexOf(Constants.INSYNC) != -1 &&
                        label1.toString().indexOf(Constants.OUTSYNC) != -1) {
                        return true;
                }
                return false;
        }



        public static boolean isSyncTask(Object label) {

                return  (label.toString().indexOf(Constants.INSYNC) != -1 ||
                         label.toString().indexOf(Constants.OUTSYNC) != -1);

        }

        public static Point getXY(String vertex, mxGraph g) {

                Object [] arr = new Object[1];
                arr[0] = g.getDefaultParent();
                Object[] cells = g.getCellsForGroup(arr);
                cells = g.getAllEdges(cells);

                System.out.println("VERTEX:"+vertex);
                for(int i = 0; i <= cells.length-1; i++) {
                        mxCell newCell = (mxCell)cells[i];
                        String vertex1 = (newCell.getSource()).getValue().toString();
                        String vertex2 = (newCell.getTarget()).getValue().toString();
                        System.out.println("CELL:"+vertex1+",CELL:"+vertex2);

                        if(vertex1.equals(vertex)) {
                                return new Point((newCell.getSource()).getGeometry().toString());
                        }
                        else if(vertex2.equals(vertex)) {
                                return new Point((newCell.getSource()).getGeometry().toString());
                        }
                }

                return null;
        }


        public static String getSymbolOnly(String operation) {

                return operation.substring(0,operation.indexOf('('));
        }

	public static File getSelectedFile(File file, JFrame frame) {

		if(file == null) {
                	JFileChooser fc = new JFileChooser() {

                        @Override
                        public void approveSelection() {
                        	File f = getSelectedFile();
                                if(f.exists() && getDialogType() == SAVE_DIALOG) {
                                	int result = JOptionPane.showConfirmDialog(frame,
                                    		"The file already exists. Do you want to replace it?",
                                                "Save",
                                                JOptionPane.YES_NO_CANCEL_OPTION);
                                	switch(result) {
                                        	case JOptionPane.YES_OPTION:
                                                	super.approveSelection();
                                                        return;
                                                case JOptionPane.NO_OPTION:
                                                	return;
                                                case JOptionPane.CLOSED_OPTION:
                                                	return;
                                                case JOptionPane.CANCEL_OPTION:
                                                	cancelSelection();
                                                        return;
                                                }
                                  	}
                                        super.approveSelection();
                   		}
                	};

			
                     	fc.setCurrentDirectory(new File("."));
                        FileNameExtensionFilter filter = new FileNameExtensionFilter("Work Order Projects (*.wop)", "wop", "wop");   
                        fc.setFileFilter(filter);
                        int result = fc.showSaveDialog(frame);
                        if (result != JFileChooser.APPROVE_OPTION) {
                        	System.out.println("You cancelled the choice");
                        	return null;
                        }

                        file = fc.getSelectedFile();
        	}

		return file;
	}
}
