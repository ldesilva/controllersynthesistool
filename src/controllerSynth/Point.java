package controllerSynth;
import java.util.*;

public class Point {

	private String x;
	private String y;
	private String width;
	private String height;

	Point(String points) {

		StringTokenizer tok = new StringTokenizer(points, "[],");
		while(tok.hasMoreTokens()) {
			String tokStr = tok.nextToken();	

			if(tokStr.indexOf("x=") != -1) {
				x = tokStr.substring(2);		
			}
			else if(tokStr.indexOf("y=") != -1) {
				y = tokStr.substring(2);		
			}
			else if(tokStr.indexOf("w=") != -1) {
				width = tokStr.substring(2);		
			}
			else if(tokStr.indexOf("h=") != -1) {
				height = tokStr.substring(2);		
			}
		}
	}

	public double getX() { return Double.parseDouble(x); }

	public double getY() { return Double.parseDouble(y); }

	public String getWidth() { return width; }

	public String getHeight() { return height; }

	@Override
	public String toString() {
		return "[x="+x+",y="+y+",w="+width+",h="+height+"]";
	}
}
