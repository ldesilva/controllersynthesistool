package controllerSynth;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import java.nio.file.*;
import javax.swing.*;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import java.io.File;

public class CreateOpRequest {

	private Document operationsRequest = null;
	private static int uniqueID = 1;
	private static int uniqueTransID = 1;

	/*
 	 * Create an operations request for matching 'in' an
 	 * 'out' transfer operations. This is done by using
 	 * standard XML-manipulation functions. These String 
 	 * constants need to be moved to the Constants.java
 	 * class.
 	 */

	public CreateOpRequest(String filename, int resNumIn, int resNumOut) {

                try {
                        DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
                        domFactory.setIgnoringComments(false);
                        DocumentBuilder builder = domFactory.newDocumentBuilder();
                        operationsRequest = builder.parse(new File(filename));

                        NodeList nodes1 = operationsRequest.getElementsByTagName("SegmentParameter");
                        Element desc = operationsRequest.createElement("Description");
                        desc.appendChild(operationsRequest.createTextNode("Transfer of parts from Resource" +
										resNumOut + " to Resource" + resNumIn));
			nodes1.item(0).getParentNode().insertBefore(desc, nodes1.item(0));

                        NodeList nodes = operationsRequest.getElementsByTagName("MaterialRequirement");
			NodeList nodesDesc = operationsRequest.getElementsByTagName("Description");

                        Element rootElementIn = operationsRequest.createElement("EquipmentRequirement");

                        Element eqIdIn = operationsRequest.createElement("EquipmentID");
                        eqIdIn.appendChild(operationsRequest.createTextNode("Resource"+resNumIn));
                        rootElementIn.appendChild(eqIdIn);

                        Element rootElementOut = operationsRequest.createElement("EquipmentRequirement");

                        Element eqIdOut = operationsRequest.createElement("EquipmentID");
                        eqIdOut.appendChild(operationsRequest.createTextNode("Resource"+resNumOut));
                        rootElementOut.appendChild(eqIdOut);

                        Element idOpReq = operationsRequest.createElement("ID");
                        idOpReq.appendChild(operationsRequest.createTextNode("Transfer #"+uniqueTransID));
                        uniqueTransID++;

			nodes.item(0).getParentNode().insertBefore(rootElementOut, nodes.item(0));
			nodes.item(0).getParentNode().insertBefore(rootElementIn, nodes.item(0));
			nodesDesc.item(1).getParentNode().insertBefore(idOpReq, nodesDesc.item(1));

      		} catch (Exception e) {
         		System.out.println("\nB2MML file "+filename+" not found");
      		}
	}




	/*
         * Create an operations request and assign resource 'resNum'
         * to execute it.
         */
         
	public CreateOpRequest(String filename, int resNum) {

		try {

			DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance(); 
			domFactory.setIgnoringComments(false);
			DocumentBuilder builder = domFactory.newDocumentBuilder(); 
			operationsRequest = builder.parse(new File(filename)); 

			NodeList nodes = operationsRequest.getElementsByTagName("MaterialRequirement");
			NodeList nodesDesc = operationsRequest.getElementsByTagName("Description");

			Element rootElement = operationsRequest.createElement("EquipmentRequirement"); 

         		Element eqId= operationsRequest.createElement("EquipmentID");
         		eqId.appendChild(operationsRequest.createTextNode("Resource"+resNum));
         		rootElement.appendChild(eqId);

         		Element eqReq = operationsRequest.createElement("EquipmentRequirementProperty");
         		Element id = operationsRequest.createElement("ID");
         		id.appendChild(operationsRequest.createTextNode("Loader"));

         		Element idOpReq = operationsRequest.createElement("ID");
         		idOpReq.appendChild(operationsRequest.createTextNode("OperationsRequest #"+uniqueID));
			uniqueID++;

			eqReq.appendChild(id);
			rootElement.appendChild(eqReq);

			nodes.item(0).getParentNode().insertBefore(rootElement, nodes.item(0));

			// Add the operation request's unique ID immediately
			// before the second 'Description' tag
			nodesDesc.item(1).getParentNode().insertBefore(idOpReq, nodesDesc.item(1));

      		} catch (Exception e) {

			Path p = Paths.get(filename);
			String file = p.getFileName().toString();
			file = file.replaceFirst("[.][^.]+$", ""); // remove file extension

                	JOptionPane.showConfirmDialog(null, 
				"The Operations Template for \""+file+"\" has not been completed.\n Continuing with the remaining operations...",
                          	"Confirm", JOptionPane.DEFAULT_OPTION);
      		}
   	}




	/*
 	 * This method is never used, 
 	 * but it probably should be.
 	 */

	public void resetCounters() {
        	uniqueID = 1;
        	uniqueTransID = 1;
	}




	public Document getOpReq() {
		return operationsRequest;
	}
}


