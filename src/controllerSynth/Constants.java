package controllerSynth;
import java.awt.*;

public class Constants {

        public static final long DELETE_CELL_TIME = 1500;
        public static final String INSYNC = "in:";
        public static final String OUTSYNC = "out:";
        public static final String IN_OUT_COLOR = "fontColor=#DC143C";
        public static final String NOP_COLOR = "fontColor=#00C5CD";
        public static final String DEFAULT_COLOR = "fontColor=#000000";
        public static final String INITIAL_STATE = "initial";
        public static final String DEFAULT_STATE = "default";
        public static final String B2MMLfolder = "data/b2mml/";
        public static final String TEMPLATE_FILE = "template.xml";
        public static final String defaultVertex = "state";
        public static final String defaultEdgeRecipe = "operations?";
        public static final String TOOLKIT_NAME = "Workorder";
        public static final int MAX_RESOURCES = 20;
        public static final String ADD_RESOURCE_TAB = "Add Resource";
	public static final int EDGE_FONTSIZE = 17;
	public static final int TOPOLOGY_EDGE_FONTSIZE = 17;
	public static final double LEFT_PANEL_PERCENTAGE = 0.75;
	public static final int DEFAULT_VERTEX_WIDTH = 100;
	public static final int WIDTH_PER_CHAR = 10;
	public static final int DEFAULT_VERTEX_HEIGHT = 50;
	public static final int TOPOLOGY_VERTEX_HEIGHT = 30;
	//public static final int CONTROLLER_VERTEX_HEIGHT = 50;
	public static final String UI_FONT = "Arial";
	public static final int UI_FONT_SIZE = 17;
	public static final int TOPOLOGY_Y_OFFSET = 100;
	public static final int CONTROLLER_Y_OFFSET = 200;
	public static final String EDGE_FONT = "Arial";
	public static final String RESOURCE = "resource";
	public static final String RECIPE_RESOURCES_WINDOW = "Recipe / Resources / Topology / Controller ";
	public static final String B2MML_WINDOW = "B2MML";
	public static final String PARAMDELIMITERS = "()";
	public static final String INITIAL_STATE_STR = "state0"; 
	public static final int NUM_OF_PARAM_CLASSES = 2;
	public static final String RECIPE = "recipe";
	public static final String TOPOLOGY = "topology";
	public static final String VERTEX_FONT = "Arial";
	public static final double TOPOLOGY_LABEL_SIZE_RATIO = 0.6;
	public static final double TOPOLOGY_EDGE_RATIO = 1.2;
	public static final double CONTROLLER_EDGE_RATIO = 1.0;
	public static final int VERTEX_FONTSIZE = 17;
	public static final int TOPOLOGY_VERTEX_FONTSIZE = 9;
        public static final String DEFAULT_VERTEX = "DEFAULT_VERTEX";
        public static final String TOPOLOGY_VERTEX_STYLE = "TOPOLOGY_VERTEX";
        public static final String TOPOLOGY_EDGE_STYLE = "TOPOLOGY_EDGE"; 
        public static final int SIMU_RELATION = 0;
        public static final int CONTROLLER = 1;
        public static final String INITIAL_VERTEX = "INITIAL_VERTEX";
        public static final String SEPARATOR = ";";
        public static final String PARALLEL_SYMBOL = "||";
        public static final String DEFAULT_EDGE = "DEFAULT_EDGE";
        public static final String NOP_EDGE = "NOP_EDGE";
        public static final String EMPTYSTRING = "";
	public static final String RESOURCE_COUNT = "RESOURCE COUNT";
	public static final String START_OF_RECIPE_VERTICES_IN_FILE = "RECIPE VERTICES";
	public static final String START_OF_RECIPE_EDGES_IN_FILE = "RECIPE EDGES";
	public static final String START_OF_RESOURCE_VERTICES_IN_FILE = "RESOURCE VERTICES";
	public static final String START_OF_RECIPE_EDGE_IN_FILE = "RECIPE EDGE";
	public static final String START_OF_RESOURCE_EDGE_IN_FILE = "RESOURCE EDGE";
	public static final String START_OF_RESOURCE_EDGES_IN_FILE = "RESOURCE EDGES";
        public static final String EDGE_SYNTAX_ERROR = "EDGE_SYNTAX_ERROR";
        public static final String SYNTAX_ERR_GUI_LABEL = "Syntax Error";
        public static final String RECIPE_TAB = "Recipe";
        public static final String RESOURCE_TAB = "Resource";
        public static final String TOPOLOGY_TAB = "Topology";
        public static final String CONTROLLER_TAB = "Controller";
        public static final int PARALLEL_EDGE_OFFSET = 80;
	public static final String startXML = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>";
	public static final String OP_REQ_XML = "OperationsRequest";
	public static final String OP_TYP_XML = "OperationsType";
	public static final String OP_ID_XML = "ID";
	public static final String OP_DESC_XML = "Description";
	public static final String OP_SEGREQ_XML = "SegmentRequirement";
	public static final String OP_SEGPARAM_XML = "SegmentParameter";
	public static final String OP_VALSTR_XML = "ValueString";
	public static final String OP_VAL_XML = "Value";
	public static final String OP_MATREQ_XML = "MaterialRequirement";
	public static final String OP_MATREQP_XML = "MaterialRequirementProperty";
	public static final String OP_MATUSE_XML = "MaterialUse";
	public static final String OP_QTY_XML = "Quantity";
	public static final String OP_SCHED_XML = "OperationsSchedule";
	public static final String PUB_DATE_XML = "PublishedDate";
	public static final String TRANSFER_B2MML = "transfer.xml";
	public static final String B2MML_EXT = ".xml";

        //public static final String OP_SYNTAX_REGEX = "(\\w+)[(][(\\w+,)]*(\\w)[)][(][\\w+,]*(\\w)[)]";
        
        public static final String PARAM_OP_SYNTAX_REGEX = "\\w+\\(((\\w+,)*\\w+)*\\)\\((\\w+,)*\\w+\\)";
        public static final String PARALLEL_OP_SYNTAX_REGEX = "("+PARAM_OP_SYNTAX_REGEX+"\\s*\\|\\|\\s*)*"+PARAM_OP_SYNTAX_REGEX;
        public static final String OP_EXPR_SYNTAX_REGEX = "("+PARALLEL_OP_SYNTAX_REGEX+"\\s*;\\s*)*"+PARALLEL_OP_SYNTAX_REGEX;

        public static final String OP_SYNTAX_REGEX = "\\w+";
        public static final String IN_OUT_EDGE = "IN_OUT_EDGE";
        public static final String b2mmlViewer = "B2MML Viewer"; 
        public static final String NOP = "nop";
        public static final String defaultEdge = NOP;
        public static final String TITLE = "Work Order Development Environment"; 
        //public static final Color SYNTAX_ERR_COLOR = Color.decode("#DC143C");
        public static final Color SYNTAX_ERR_COLOR = Color.decode("#B22222");
        public static final String ALERTS_GUI_LABEL = "Alerts:"; 
        public static final String MANUF_GUI_LABEL = "Manufacturable"; 

        //public static final Color SYNTAX_ERR_COLOR = Color.RED;
        
        public static final Color MANUF_COLOR = Color.decode("#9ACD32");
        public static final Color NOT_MANUF_COLOR = SYNTAX_ERR_COLOR; 
        

        /**
         * Jack's Edits
         */
        //Unobservable actions are specified by prefixing them with an underscore. e.g. _attGr1.
        public static final String UNOBSERVABLE_PREFIX = "_";
        public static final String UNOBSERVABLE_EDGE = "UNOBSERVABLE_EDGE";
        /**
         * ----
         */
}
